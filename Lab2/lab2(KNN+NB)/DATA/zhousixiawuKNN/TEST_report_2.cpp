#include<iostream>
#include<iomanip>
#include<fstream>
#include<cstring>
#include<string>
#include<vector>
#include<unordered_set>
#include<algorithm>
#include<cmath>
#include<stdlib.h>
using namespace std;

struct Data{
	string sentence;
	string label;
};

struct Emotion{
	string e;
	vector <string> words;
	vector <string> non_rep;//不重复 
	double count;
};


string LIST = "E:/HomeWork/Grade_3_Last/AI/Lab2/lab2(KNN+NB)/DATA/zhousixiawuKNN/";
vector <Data> train_set;	//储存所有分类数据训练文本
vector <string> words;  //储存所有分类数据词汇
Emotion emo[2];			//六种情感具有的词数，句数 

void initial();
void cut(string file);
double all_words;
void NB(string file,int type);

int main(){
	initial();
	cut("train_set.csv");
	NB("test_set.csv",1);
	return 0;
}

void initial(){
	all_words=0;
	for(int i=0;i<2;i++)	emo[i].count=0;
	emo[0].e="joy";
	emo[1].e="sad";
}

void read(string file,vector<Data> &set,int type){
	ifstream f(LIST+file);
	string data;
	while(getline(f,data)){				//读取文本并切割储存 
		if(type!=1) data.erase(0,data.find(",",0)+1);
		Data s;
		s.sentence=" "+data.substr(0,data.find(",",0))+" ";		//利用文本中的逗号进行来分割文本 
		s.label=data.substr(data.find(",",0)+1,data.size()-data.find(",",0));
		if(s.label!="label")	set.push_back(s);
	}
	f.close();
}

void cut(string file){
	read(file,train_set,1);
	for(int i=0;i<train_set.size();i++){
		int em=0;
		for(int j=0;j<2;j++){		//找到对应的label号 
			if(train_set[i].label==emo[j].e){
				em=j;
				break;
			}
		}
		int start=1,end=train_set[i].sentence.find(" ",start);		//因为前面添加了一个空格，所以从1号位开始查找空格 
		while(end!=-1){
			string substr=train_set[i].sentence.substr(start,end-start);
			start=end+1;								//从下一个空格后开始继续查找空格 
			end=train_set[i].sentence.find(" ",start);
			
			emo[em].count++;
			emo[em].words.push_back(substr);
			all_words++;
			//插入对应情感中不重复的词 
			if(find(emo[em].non_rep.begin(),emo[em].non_rep.end(),substr)==emo[em].non_rep.end())
				emo[em].non_rep.push_back(substr);
			//利用find函数查找该词是否已经存在，如果不存在就储存到vector中 
			if(find(words.begin(),words.end(),substr)==words.end()) 
				words.push_back(substr);
		}		 
	}
}

void NB(string file,int type){
	vector <Data> set;
	read(file,set,type);
	double correct=0;
	ofstream f(LIST+"classification_dataset/15352013_Sample_NB_classification.csv",ios::trunc);
	for(int i=0;i<set.size();i++){
		double rate[2];	//每一个测试文本的6个情感概率 
		for(int em=0;em<2;em++){
			rate[em]=emo[em].count/all_words;	//初始情感概率为：情感词数/总词数 
			cout<<"情感 "<<emo[em].e<<"的概率为：";
			//cout<<"("<<emo[em].count<<"/"<<all_words<<")";
			int start=1,end=set[i].sentence.find(" ",start);
			//因为前面添加了一个空格，所以从1号位开始查找空格 
			while(end!=-1){
				string substr=set[i].sentence.substr(start,end-start);
				start=end+1;					//从下一个空格后开始继续查找空格 
				end=set[i].sentence.find(" ",start);
				//找出训练集同情感的相同词的个数 
				double nwexk=0;
				for(int j=0;j<emo[em].words.size();j++){
					if(emo[em].words[j]==substr) nwexk++;
				}	
				//拉普拉斯平滑
				//cout<<"*[("<<nwexk<<"+1.0)/("<<emo[em].non_rep.size()<<"+"<<words.size()<<")]";
				rate[em]*=(nwexk+1.0)/double(emo[em].non_rep.size()+words.size());
			}
			cout<<endl<<rate[em]<<endl;		 
		}
		double max=0;
		string label;
		for(int em=0;em<2;em++){
			if(rate[em]>=max){
				max=rate[em];
				label=emo[em].e;
			}
		}
		cout<<"最终语句："<<set[i].sentence<<" 判定为："<<label<<endl;
		if(type!=1)	f<<i+1<<","<<label<<endl;
		if(label==set[i].label) correct++;
	}
	//cout<<correct/(double(set.size()))<<endl;
}
