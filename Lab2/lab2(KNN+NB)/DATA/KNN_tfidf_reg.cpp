#include<iostream>
#include<iomanip>
#include<fstream>
#include<cstring>
#include<string>
#include<vector>
#include<unordered_set>
#include<algorithm>
#include<cmath>
#include<stdlib.h>
using namespace std;

struct Data{
	string sentence;
	double label[6];
};

string LIST = "E:/HomeWork/Grade_3_Last/AI/Lab2/lab2(KNN+NB)/DATA/regression_dataset/";
vector <Data> train_set;	//储存所有回归数据训练文本 
vector <string> words;	//储存所有回归数据词汇
vector <string> emotion; 	//存储所有情感

double tfidf[1000][4000];
int best_K=11;

void cut(string file);
void TFIDF();
void KNN(string file,int type);
int main(){
	cut("train_set.csv");
	TFIDF();
	KNN("validation_set.csv",1);
	KNN("test_set.csv",2);
	return 0;
}

void TFIDF(){
	////回归: 
	for(int i=0;i<train_set.size();i++){
		double words_count=0;
		int start=1,end=train_set[i].sentence.find(" ",start);//因为前面添加了一个空格，所以从1号位开始查找空格 
		while(end!=-1){
			start=end+1;								//从下一个空格后开始继续查找空格 
			end=train_set[i].sentence.find(" ",start);
			words_count++;
		}
		for(int j=0;j<words.size();j++){
			double count = 0;
			start=train_set[i].sentence.find(" "+words[j]+" ",0);
			while(start!=-1){							//查找该词在文本中出现的次数 
				count++;
				start = train_set[i].sentence.find(" "+words[j]+" ",start+1);
			}
			tfidf[i][j]=count/words_count;				//记录tf矩阵
		}
	}
	vector <double> user_count;
	for(int i=0;i<words.size();i++){					//计算各个词汇被使用的文本数 
		double count=0;
		for(int j=0;j<train_set.size();j++)
			if(train_set[j].sentence.find(" "+words[i]+" ")!=-1)	count++;
		user_count.push_back(count);
	}
	double all=train_set.size();
	for(int i=0;i<train_set.size();i++){					//输出TF-IDF矩阵 
		for(int j=0;j<words.size();j++){
			tfidf[i][j]=tfidf[i][j]*log(all/(1.0+user_count[j]))/log(2.0);
		}
	}
	for(int i=0;i<train_set.size();i++){
		double mu=0;
		for(int j=0;j<words.size();j++)
			mu+=tfidf[i][j];
		mu/=double(words.size());
		double sigma=0;
		for(int j=0;j<words.size();j++)
			sigma+=(tfidf[i][j]-mu)*(tfidf[i][j]-mu);
		sigma=sqrt(sigma/double(words.size()));
		for(int j=0;j<words.size();j++)
			tfidf[i][j]=(tfidf[i][j]-mu)/sigma;
	}
}

double TFIDF_set[1000][4000];

void read(string file,vector<Data> &set,int type){
	ifstream f(LIST+file);
	string data;
	while(getline(f,data)){				//读取文本并切割储存 
		if(type!=1) data.erase(0,data.find(",",0)+1);
		Data s;
		data+=",";
		s.sentence=" "+data.substr(0,data.find(",",0))+" ";		//利用文本中的逗号进行来分割文本 
		int start=data.find(",",0),end=data.find(",",start+1);
		for(int i=0;i<6;i++){
			string sub=data.substr(start+1,end-start-1);
			s.label[i]=strtod(sub.c_str(),NULL);
			start=end;
			end=data.find(",",start+1);
		}
		if(s.sentence!=" Words (split by space) ") set.push_back(s);
	}
	f.close();
}

void cut(string file){
	read(file,train_set,1);
	for(int i=0;i<train_set.size();i++){
		int start=1,end=train_set[i].sentence.find(" ",start);		//因为前面添加了一个空格，所以从1号位开始查找空格 
		while(end!=-1){
			string substr=train_set[i].sentence.substr(start,end-start);
			start=end+1;								//从下一个空格后开始继续查找空格 
			end=train_set[i].sentence.find(" ",start);
			//利用find函数查找该词是否已经存在，如果不存在就储存到vector中 
			if(find(words.begin(),words.end(),substr)==words.end()) 
				words.push_back(substr);
		}			 
	}
}

struct Distance_r{
	double dis;
	double label[6];
};

bool cmp_r(Distance_r a,Distance_r b){
	return a.dis<b.dis;
}

void KNN(string file,int type){
	vector <Data> set;
	read(file,set,type);
	for(int i=0;i<set.size();i++){
		double words_count=0;
		int start=1,end=set[i].sentence.find(" ",start);//因为前面添加了一个空格，所以从1号位开始查找空格 
		while(end!=-1){
			start=end+1;								//从下一个空格后开始继续查找空格 
			end=set[i].sentence.find(" ",start);
			words_count++;
		}
		for(int j=0;j<words.size();j++){
			double count = 0;
			start=set[i].sentence.find(" "+words[j]+" ",0);
			while(start!=-1){							//查找该词在文本中出现的次数 
				count++;
				start = set[i].sentence.find(" "+words[j]+" ",start+1);
			}
			TFIDF_set[i][j]=count/words_count;				//记录tf矩阵
		}
	}
	vector <double> user_count;
	for(int i=0;i<words.size();i++){					//计算各个词汇被使用的文本数 
		double count=0;
		for(int j=0;j<train_set.size();j++)
			if(train_set[j].sentence.find(" "+words[i]+" ")!=-1)	count++;
		user_count.push_back(count);
	}
	double all=train_set.size();
	for(int i=0;i<set.size();i++){					//输出TF-IDF矩阵 
		for(int j=0;j<words.size();j++){
			TFIDF_set[i][j]=TFIDF_set[i][j]*log(all/(1.0+user_count[j]))/log(2.0);
		}
	}
	for(int i=0;i<set.size();i++){
		double mu=0;
		for(int j=0;j<words.size();j++)
			mu+=TFIDF_set[i][j];
		if(mu==0) mu=0.01;
		mu/=double(words.size());
		double sigma=0;
		for(int j=0;j<words.size();j++)
			sigma+=(TFIDF_set[i][j]-mu)*(TFIDF_set[i][j]-mu);
		sigma=sqrt(sigma/double(words.size()));
		for(int j=0;j<words.size();j++){
			double t=TFIDF_set[i][j];
			TFIDF_set[i][j]=(TFIDF_set[i][j]-mu)/sigma;
		}
	}
	vector<vector <Distance_r>> all_dis;
	for(int i=0;i<set.size();i++){
		vector <Distance_r> d;
		for(int j=0;j<train_set.size();j++){
			double tmp=0;
			for(int w=0;w<words.size();w++){
				tmp+=(TFIDF_set[i][w]-tfidf[j][w])*(TFIDF_set[i][w]-tfidf[j][w]);
			}
			Distance_r tmp2;
			if(tmp==0)	tmp2.dis=1;
			else tmp2.dis=sqrt(tmp);
			for(int w=0;w<6;w++)
				tmp2.label[w]=train_set[j].label[w];
			d.push_back(tmp2);
		}
		sort(d.begin(),d.end(),cmp_r);
		all_dis.push_back(d);
	}
	if(type == 1){ //验证集 
		string ss[23]={"3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25"};
		for(int K=3;K<26;K++){
			double result[1000][6];
			for(int i=0;i<set.size();i++){
				for(int j=0;j<6;j++){
					double rate=0;
					for(int w=0;w<K;w++){
						rate+=all_dis[i][w].label[j]/all_dis[i][w].dis;
					}
					result[i][j]=rate;
				}
			}
			for(int i=0;i<set.size();i++){
				double sum = 0;
				for(int j=0;j<6;j++) sum+=result[i][j];
				for(int j=0;j<6;j++) result[i][j]/=sum;
			}
			ofstream f(LIST+"set_"+ss[K-3]+".csv",ios::trunc);
			for(int i=0;i<set.size();i++){
				for(int j=0;j<6;j++) f<<result[i][j]<<",";
				f<<endl;
			}
			f.close();
		}
	}else{
		double result[1000][6];
		for(int i=0;i<set.size();i++){
			for(int j=0;j<6;j++){
				double rate=0;
				for(int w=0;w<best_K;w++)
					rate+=all_dis[i][w].label[j]/all_dis[i][w].dis;
				result[i][j]=rate;
			}
		}
		for(int i=0;i<set.size();i++){	//情感概率归一化 
			double sum = 0;
			for(int j=0;j<6;j++) sum+=result[i][j];
			for(int j=0;j<6;j++) result[i][j]/=sum;
		}
		ofstream f(LIST+"15352013_Sample_KNN_regression.csv",ios::trunc);
		f<<"textid,anger,disgust,fear,joy,sad,surprise"<<endl;
		for(int i=0;i<set.size();i++){
			f<<i+1<<",";
			for(int j=0;j<6;j++) f<<result[i][j]<<",";
			f<<endl;
		}
		f.close();
	}
}
