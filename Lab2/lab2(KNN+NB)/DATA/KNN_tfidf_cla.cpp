#include<iostream>
#include<iomanip>
#include<fstream>
#include<cstring>
#include<string>
#include<vector>
#include<unordered_set>
#include<algorithm>
#include<cmath>
#include<stdlib.h>
using namespace std;

struct Data{
	string sentence;
	string label;
};

string LIST = "E:/HomeWork/Grade_3_Last/AI/Lab2/lab2(KNN+NB)/DATA/classification_dataset/";
vector <Data> train_set;	//储存所有分类数据训练文本
vector <string> words;  //储存所有分类数据词汇
vector <string> emotion; 	//存储所有情感

double tfidf[1000][4000];//记录tfidf矩阵 
int best_K=10;//记录最优的K值，默认10 

void cut(string file); //文本预处理 
void TFIDF();	//生成TFIDF矩阵 
void KNN(string file,int type);	//测试（验证）文本判决 

int main(){
	cut("train_set.csv");
	TFIDF();
	KNN("validation_set.csv",1);//验证 
	KNN("test_set.csv",2);//测试 
	return 0;
}

void read(string file,vector<Data> &set,int type){	//读取文本 
	ifstream f(LIST+file);
	string data;
	while(getline(f,data)){				//读取文本并切割储存 
		if(type!=1) data.erase(0,data.find(",",0)+1);
		Data s;
		s.sentence=" "+data.substr(0,data.find(",",0))+" ";		//利用文本中的逗号进行来分割文本 
		s.label=data.substr(data.find(",",0)+1,data.size()-data.find(",",0));
		if(s.label!="label")	set.push_back(s);
	}
	f.close();
}

void cut(string file){
	read(file,train_set,1);
	for(int i=0;i<train_set.size();i++){
		int start=1,end=train_set[i].sentence.find(" ",start);	//因为前面添加了一个空格，所以从1号位开始查找空格 
		while(end!=-1){
			string substr=train_set[i].sentence.substr(start,end-start);
			start=end+1;								//从下一个空格后开始继续查找空格 
			end=train_set[i].sentence.find(" ",start);
			//利用find函数查找该词是否已经存在，如果不存在就储存到vector中 
			if(find(words.begin(),words.end(),substr)==words.end()) 
				words.push_back(substr);
		}
		//存储情感种类，其实就是6种情感 
		if(find(emotion.begin(),emotion.end(),train_set[i].label)==emotion.end())	
			emotion.push_back(train_set[i].label);				 
	}
}

void TFIDF(){	//生成TFIDF矩阵 
	for(int i=0;i<train_set.size();i++){
		double words_count=0;
		int start=1,end=train_set[i].sentence.find(" ",start);//因为前面添加了一个空格，所以从1号位开始查找空格 
		while(end!=-1){
			start=end+1;								//从下一个空格后开始继续查找空格 
			end=train_set[i].sentence.find(" ",start);
			words_count++;			//记录该训练文本中共有多少个词 
		}
		for(int j=0;j<words.size();j++){
			double count = 0;
			start=train_set[i].sentence.find(" "+words[j]+" ",0);
			while(start!=-1){							//查找该词在文本中重复出现的次数 
				count++;
				start = train_set[i].sentence.find(" "+words[j]+" ",start+1);
			}
			tfidf[i][j]=count/words_count;				//记录tf矩阵
		}
	}
	vector <double> user_count;	
	for(int i=0;i<words.size();i++){					//计算各个词汇被使用的文本数 
		double count=0;
		for(int j=0;j<train_set.size();j++)
			if(train_set[j].sentence.find(" "+words[i]+" ")!=-1)	count++;
		user_count.push_back(count);
	}
	double all=train_set.size();
	for(int i=0;i<train_set.size();i++){					//输出TF-IDF矩阵 
		for(int j=0;j<words.size();j++){
			tfidf[i][j]=tfidf[i][j]*log(all/(1.0+user_count[j]))/log(2.0);
		}
	}
	user_count.clear();
	
	//TFIDF的每一行零均值标准化
	for(int i=0;i<train_set.size();i++){
		double mu=0;//均值 
		for(int j=0;j<words.size();j++)
			mu+=tfidf[i][j];
		mu/=double(words.size());
		double sigma=0;//标准差 
		for(int j=0;j<words.size();j++)
			sigma+=(tfidf[i][j]-mu)*(tfidf[i][j]-mu);
		sigma=sqrt(sigma/double(words.size()));
		for(int j=0;j<words.size();j++)
			tfidf[i][j]=(tfidf[i][j]-mu)/sigma;//tfidf矩阵标准化 
	}
}

struct Distance{	//记录距离 
	double dis;
	string label;
};

bool cmp_(Distance a,Distance b){ //排序cmp 
	return a.dis<b.dis;
}

double TFIDF_set[1000][4000];//测试集（验证集）的TFIDF矩阵 

void KNN(string file,int type){
	vector <Data> set;
	read(file,set,type);
	for(int i=0;i<set.size();i++){	//建立测试或训练文本的TFIDF矩阵 
		double words_count=0;
		int start=1,end=set[i].sentence.find(" ",start);//因为前面添加了一个空格，所以从1号位开始查找空格 
		while(end!=-1){
			start=end+1;								//从下一个空格后开始继续查找空格 
			end=set[i].sentence.find(" ",start);
			words_count++;
		}
		for(int j=0;j<words.size();j++){
			double count = 0;
			start=set[i].sentence.find(" "+words[j]+" ",0);
			while(start!=-1){							//查找该词在文本中出现的次数 
				count++;
				start = set[i].sentence.find(" "+words[j]+" ",start+1);
			}
			TFIDF_set[i][j]=count/words_count;				//记录tfidf矩阵
		}
	}
	vector <double> user_count;		
	for(int i=0;i<words.size();i++){					//计算各个词汇被使用的文本数 
		double count=0;
		for(int j=0;j<train_set.size();j++)
			if(train_set[j].sentence.find(" "+words[i]+" ")!=-1)	count++;
		user_count.push_back(count);
	}
	double all=train_set.size()+1;
	for(int i=0;i<set.size();i++){					//输出TF-IDF矩阵 
		for(int j=0;j<words.size();j++){
			TFIDF_set[i][j]=TFIDF_set[i][j]*log(all/(1.0+user_count[j]))/log(2.0);
		}
	}
	//零均值标准化 
	for(int i=0;i<set.size();i++){
		double mu=0;
		for(int j=0;j<words.size();j++)
			mu+=TFIDF_set[i][j];
		mu/=double(words.size());
		if(mu==0) mu=0.01;
		double sigma=0;
		for(int j=0;j<words.size();j++)
			sigma+=(TFIDF_set[i][j]-mu)*(TFIDF_set[i][j]-mu);
		sigma=sqrt(sigma/double(words.size()));
		for(int j=0;j<words.size();j++)
			TFIDF_set[i][j]=(TFIDF_set[i][j]-mu)/sigma;
	}
	
	//计算距离，使用欧式距离 
	vector<vector <Distance>> all_dis;
	for(int i=0;i<set.size();i++){	//每一句测试集进行计算： 
		vector <Distance> d;
		for(int j=0;j<train_set.size();j++){	//测试文本与训练集的各个距离 
			double tmp=0,sum1=0,sum2=0;
			for(int w=0;w<words.size();w++){	//欧式距离 
				tmp+=(TFIDF_set[i][w]-tfidf[j][w])*(TFIDF_set[i][w]-tfidf[j][w]);
			}
			Distance tmp2;
			tmp2.dis=sqrt(tmp);
			tmp2.label=train_set[j].label;
			d.push_back(tmp2);
		}
		sort(d.begin(),d.end(),cmp_);	//距离排序 
		all_dis.push_back(d);
	}
	
	if(type==1){ //验证集寻找最佳K值 
		double best_correct = -1;
		for(int K=1;K<sqrt(train_set.size())+4;K++){
			double correct = 0;
			for(int i=0;i<set.size();i++){
				int count[50]={0};
				for(int j=0;j<K;j++){
					for(int w=0;w<emotion.size();w++){
						if(all_dis[i][j].label==emotion[w]){
							count[w]++;
							break;
						}
					}
				}
				int max=-1,at=-1;
				for(int j=0;j<emotion.size();j++){
					if(count[j]>=max){
						max=count[j];
						at=j;
					}
				}
				if(set[i].label==emotion[at]) correct++;
			}
			cout<<K<<"  "<<correct/(double(set.size()))<<endl;
			if(correct>best_correct){
				best_correct=correct;
				best_K=K;
			}
		}
	}else{ //输出测试集结果 
		ofstream f(LIST+"15352013_Sample_KNN_classification.csv",ios::trunc);
		f<<"textid,label"<<endl;
		for(int i=0;i<set.size();i++){	//预测每个测试文本的label 
			int count[50]={0};
			for(int j=0;j<best_K;j++){	//找到前K个近邻文本的众数label 
				for(int w=0;w<emotion.size();w++){
					if(all_dis[i][j].label==emotion[w]){
						count[w]++;
						break;
					}
				}
			}
			int max=-1,at=-1;
			for(int j=0;j<emotion.size();j++){
				if(count[j]>=max){
					max=count[j];
					at=j;
				}
			}
			f<<i+1<<","<<emotion[at]<<endl;
		}
		f.close();
	}
	cout<<best_K<<endl;
}

