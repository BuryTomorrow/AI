#include<iostream>
#include<iomanip>
#include<fstream>
#include<cstring>
#include<string>
#include<vector>
#include<unordered_set>
#include<algorithm>
#include<cmath>
#include<stdlib.h>
using namespace std;

struct Data{
	string sentence;
	vector <string> words;
	double non_rep;
	double label[6];
};

string LIST = "E:/HomeWork/Grade_3_Last/AI/Lab2/lab2(KNN+NB)/DATA/regression_dataset/";

vector <Data> train_set;	//储存所有回归数据训练文本 
vector <string> words;	//储存所有回归数据词汇

void cut(string file);
void NB(string file,int type);
int main(){
	cut("train_set.csv");
	NB("validation_set.csv",1);
	NB("test_set.csv",2);
	return 0;
}

void read(string file,vector<Data> &set,int type){
	ifstream f(LIST+file);
	string data;
	while(getline(f,data)){				//读取文本并切割储存 
		if(type!=1) data.erase(0,data.find(",",0)+1);
		Data s;
		data+=",";
		s.sentence=" "+data.substr(0,data.find(",",0))+" ";		//利用文本中的逗号进行来分割文本 
		int start=data.find(",",0),end=data.find(",",start+1);
		for(int i=0;i<6;i++){
			string sub=data.substr(start+1,end-start-1);
			s.label[i]=strtod(sub.c_str(),NULL);
			start=end;
			end=data.find(",",start+1);
		}
		if(s.sentence!=" Words (split by space) ") set.push_back(s);
	}
	f.close();
}

void cut(string file){
	read(file,train_set,1);
	for(int i=0;i<train_set.size();i++){
		train_set[i].non_rep=0;
		int start=1,end=train_set[i].sentence.find(" ",start);		//因为前面添加了一个空格，所以从1号位开始查找空格 
		while(end!=-1){
			string substr=train_set[i].sentence.substr(start,end-start);
			start=end+1;								//从下一个空格后开始继续查找空格 
			end=train_set[i].sentence.find(" ",start);
			//利用find函数查找该词是否已经存在，如果不存在就储存到vector中 
			if(find(words.begin(),words.end(),substr)==words.end()) 
				words.push_back(substr);
			train_set[i].words.push_back(substr);
			if(find(train_set[i].words.begin(),train_set[i].words.end(),substr)==train_set[i].words.end()) 
				train_set[i].non_rep++;
		}			 
	}
}

void NB(string file,int type){
	vector <Data> set;
	read(file,set,type);
	string t;
	if(type==1)	t="set.csv";
	else	t="15352013_Sample_NB_regression.csv";
	ofstream f(LIST+t,ios::trunc);
	f<<"textid,anger,disgust,fear,joy,sad,surprise"<<endl;
	for(int i=0;i<set.size();i++){
		double rate[6]={0};	//六种情感的概率初始为0； 
		for(int j=0;j<6;j++){
			double r=0;		//记录总的训练集贡献，就是求和公式 
			for(int w=0;w<train_set.size();w++){
				//di为某一情感下训练文本w对测试文本i情感概率的贡献值 
				double di=train_set[w].label[j];	//初始为训练文本w的情感概率 
				int start=1,end=set[i].sentence.find(" ",start);	
				//对测试文本的每个单词进行概率积： 
				while(end!=-1){
					string substr=set[i].sentence.substr(start,end-start);
					start=end+1;								//从下一个空格后开始继续查找空格 
					end=set[i].sentence.find(" ",start);
					double count=0;//找到测试文本中词xk在训练文本w中出现的词数 
					for(int k=0;k<train_set[w].words.size();k++){
						if(train_set[w].words[k]==substr) count++;
					}
					//拉普拉斯回归平滑公式 
					di*=(count+0.033)/(double(train_set[w].words.size())+0.033*double(words.size()));
				}
				r+=di;	//求和 
			}
			rate[j]=r;
		}
		double sum=0;
		for(int j=0;j<6;j++) sum+=rate[j];
		for(int j=0;j<6;j++) rate[j]/=sum;
		if(type==1){
			for(int j=0;j<6;j++) f<<rate[j]<<",";
		}else{
			f<<i+1<<",";
			for(int j=0;j<6;j++) f<<rate[j]<<",";
		}
		f<<endl;
	}
}
