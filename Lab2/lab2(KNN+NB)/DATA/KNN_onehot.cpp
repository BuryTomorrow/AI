#include<iostream>
#include<iomanip>
#include<fstream>
#include<cstring>
#include<string>
#include<vector>
#include<unordered_set>
#include<algorithm>
#include<cmath>
#include<stdlib.h>
using namespace std;

struct Data_c{
	string sentence;
	string label;
};
struct Data_r{
	string sentence;
	double label[6];
};

string LIST = "E:/HomeWork/Grade_3_Last/AI/Lab2/lab2(KNN+NB)/DATA/";
vector <Data_c> cla_t_set;	//储存所有分类数据训练文本
vector <string> cla_words;  //储存所有分类数据词汇
vector <Data_r> reg_t_set;	//储存所有回归数据训练文本 
vector <string> reg_words;	//储存所有回归数据词汇
vector <string> emotion; 	//存储所有情感
vector <double> words_count;
double one_hot_c[1000][4000];
double one_hot_r[1000][4000];
double tf[1000][4000];

void cut_c(string file);
void cut_r(string file);
void Onehot();
//void TF();
void KNN_CSF_vali();
void KNN_REG_vali();
int main(){
	cut_c("classification_dataset/train_set.csv");
	cut_r("regression_dataset/train_set.csv");
	Onehot();
	KNN_CSF_vali();
	//KNN_REG_vali();
	return 0;
}

void read_c(string file,vector<Data_c> &set){
	ifstream f(LIST+file);
	string data;
	while(getline(f,data)){				//读取文本并切割储存 
		Data_c s;
		s.sentence=" "+data.substr(0,data.find(",",0))+" ";		//利用文本中的逗号进行来分割文本 
		s.label=data.substr(data.find(",",0)+1,data.size()-data.find(",",0));
		if(s.label!="label")	set.push_back(s);
	}
	f.close();
}

void cut_c(string file){
	read_c(file,cla_t_set);
	for(int i=0;i<cla_t_set.size();i++){
		int start=1,end=cla_t_set[i].sentence.find(" ",start);		//因为前面添加了一个空格，所以从1号位开始查找空格 
		while(end!=-1){
			string substr=cla_t_set[i].sentence.substr(start,end-start);
			start=end+1;								//从下一个空格后开始继续查找空格 
			end=cla_t_set[i].sentence.find(" ",start);
			//利用find函数查找该词是否已经存在，如果不存在就储存到vector中 
			if(find(cla_words.begin(),cla_words.end(),substr)==cla_words.end()) 
				cla_words.push_back(substr);
		}
		if(find(emotion.begin(),emotion.end(),cla_t_set[i].label)==emotion.end())	
			emotion.push_back(cla_t_set[i].label);				 
	}
}

void Onehot(){
	for(int i=0;i<cla_t_set.size();i++){
		for(int j=0;j<cla_words.size();j++){
			if(cla_t_set[i].sentence.find(" "+cla_words[j]+" ")==-1)	one_hot_c[i][j]=0;
			else one_hot_c[i][j]=1;
		}
	}
	for(int i=0;i<cla_t_set.size();i++){
		double mu=0;
		for(int j=0;j<cla_words.size();j++)
			mu+=one_hot_c[i][j];
		mu/=double(cla_words.size());
		double sigma=0;
		for(int j=0;j<cla_words.size();j++)
			sigma+=(one_hot_c[i][j]-mu)*(one_hot_c[i][j]-mu);
		sigma=sqrt(sigma/double(cla_words.size()));
		for(int j=0;j<cla_words.size();j++)
			one_hot_c[i][j]=(one_hot_c[i][j]-mu)/sigma;
	}
	for(int i=0;i<reg_t_set.size();i++){
		for(int j=0;j<reg_words.size();j++){
			if(reg_t_set[i].sentence.find(" "+reg_words[j]+" ")==-1)	one_hot_r[i][j]=0;
			else one_hot_r[i][j]=1;
		}
	}
	for(int i=0;i<reg_t_set.size();i++){
		double mu=0;
		for(int j=0;j<reg_words.size();j++)
			mu+=one_hot_r[i][j];
		mu/=double(reg_words.size());
		double sigma=0;
		for(int j=0;j<reg_words.size();j++)
			sigma+=(one_hot_r[i][j]-mu)*(one_hot_r[i][j]-mu);
		sigma=sqrt(sigma/double(reg_words.size()));
		for(int j=0;j<reg_words.size();j++)
			one_hot_r[i][j]=(one_hot_r[i][j]-mu)/sigma;
	}
}

struct Distance_c{
	double dis;
	string label;
};

bool cmp_c(Distance_c a,Distance_c b){
	return a.dis<b.dis;
}

int OH_vali[1000][4000];

void KNN_CSF_vali(){
	vector <Data_c> vali;
	read_c("classification_dataset/validation_set.csv",vali);
	for(int i=0;i<vali.size();i++){
		for(int j=0;j<cla_words.size();j++){
			if(vali[i].sentence.find(" "+cla_words[j]+" ")==-1)	OH_vali[i][j]=0;
			else OH_vali[i][j]=1;
		}
	}
	for(int i=0;i<vali.size();i++){
		double mu=0;
		for(int j=0;j<cla_words.size();j++)
			mu+=OH_vali[i][j];
		mu/=double(cla_words.size());
		double sigma=0;
		for(int j=0;j<cla_words.size();j++)
			sigma+=(OH_vali[i][j]-mu)*(OH_vali[i][j]-mu);
		sigma=sqrt(sigma/double(cla_words.size()));
		for(int j=0;j<cla_words.size();j++)
			OH_vali[i][j]=(OH_vali[i][j]-mu)/sigma;
	}
	vector<vector <Distance_c>> all_dis;
	for(int i=0;i<vali.size();i++){
		vector <Distance_c> d;
		for(int j=0;j<cla_t_set.size();j++){
			double tmp=0;
			for(int w=0;w<cla_words.size();w++){
				tmp+=(OH_vali[i][w]-one_hot_c[j][w])*(OH_vali[i][w]-one_hot_c[j][w]);
			}
			Distance_c tmp2;
			tmp2.dis=sqrt(tmp);
			tmp2.label=cla_t_set[j].label;
			d.push_back(tmp2);
		}
		sort(d.begin(),d.end(),cmp_c);
		all_dis.push_back(d);
	}
	for(int K=1;K<sqrt(cla_t_set.size())+4;K++){
		double correct = 0;
		for(int i=0;i<vali.size();i++){
			int count[50]={0};
			for(int j=0;j<K;j++){
				for(int w=0;w<emotion.size();w++){
					if(all_dis[i][j].label==emotion[w]){
						count[w]++;
						break;
					}
				}
			}
			int max=-1,at=-1;
			for(int j=0;j<emotion.size();j++){
				if(count[j]>=max){
					max=count[j];
					at=j;
				}
			}
			if(vali[i].label==emotion[at]) correct++;
			
		}
		cout<<K<<"  "<<correct/(double(vali.size()))<<endl;
	}
}

void read_r(string file,vector<Data_r> &set){
	ifstream f(LIST+file);
	string data;
	while(getline(f,data)){				//读取文本并切割储存 
		Data_r s;
		data+=",";
		s.sentence=" "+data.substr(0,data.find(",",0))+" ";		//利用文本中的逗号进行来分割文本 
		int start=data.find(",",0),end=data.find(",",start+1);
		for(int i=0;i<6;i++){
			string sub=data.substr(start+1,end-start-1);
			s.label[i]=strtod(sub.c_str(),NULL);
			start=end;
			end=data.find(",",start+1);
		}
		if(s.sentence!=" Words (split by space) ") set.push_back(s);
	}
	f.close();
}

void cut_r(string file){
	read_r(file,reg_t_set);
	for(int i=0;i<reg_t_set.size();i++){
		int start=1,end=reg_t_set[i].sentence.find(" ",start);		//因为前面添加了一个空格，所以从1号位开始查找空格 
		while(end!=-1){
			string substr=reg_t_set[i].sentence.substr(start,end-start);
			start=end+1;								//从下一个空格后开始继续查找空格 
			end=reg_t_set[i].sentence.find(" ",start);
			//利用find函数查找该词是否已经存在，如果不存在就储存到vector中 
			if(find(reg_words.begin(),reg_words.end(),substr)==reg_words.end()) 
				reg_words.push_back(substr);
		}			 
	}
}

struct Distance_r{
	double dis;
	double label[6];
};

bool cmp_r(Distance_r a,Distance_r b){
	return a.dis<b.dis;
}

void KNN_REG_vali(){
	vector <Data_r> vali;
	read_r("classification_dataset/validation_set.csv",vali);
	for(int i=0;i<vali.size();i++){
		for(int j=0;j<reg_words.size();j++){
			if(vali[i].sentence.find(" "+reg_words[j]+" ")==-1)	OH_vali[i][j]=0;
			else OH_vali[i][j]=1;
		}
	}
	for(int i=0;i<vali.size();i++){
		double mu=0;
		for(int j=0;j<reg_words.size();j++)
			mu+=OH_vali[i][j];
		mu/=double(reg_words.size());
		double sigma=0;
		for(int j=0;j<reg_words.size();j++)
			sigma+=(OH_vali[i][j]-mu)*(OH_vali[i][j]-mu);
		sigma=sqrt(sigma/double(reg_words.size()));
		for(int j=0;j<reg_words.size();j++)
			OH_vali[i][j]=(OH_vali[i][j]-mu)/sigma;
	}
	vector<vector <Distance_r>> all_dis;
	for(int i=0;i<vali.size();i++){
		vector <Distance_r> d;
		for(int j=0;j<reg_t_set.size();j++){
			double tmp=0;
			for(int w=0;w<reg_words.size();w++){
				tmp+=(OH_vali[i][w]-one_hot_r[j][w])*(OH_vali[i][w]-one_hot_r[j][w]);
			}
			Distance_r tmp2;
			tmp2.dis=max(sqrt(tmp),1.0);
			for(int w=0;w<6;w++) tmp2.label[w]=reg_t_set[j].label[w];
			d.push_back(tmp2);
		}
		sort(d.begin(),d.end(),cmp_r);
		all_dis.push_back(d);
	}
	string ss[23]={"3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25"};
	for(int K=3;K<26;K++){
		double result[1000][6];
		for(int i=0;i<vali.size();i++){
			for(int j=0;j<6;j++){
				double rate=0;
				for(int w=0;w<K;w++){
					rate+=all_dis[i][w].label[j]/all_dis[i][w].dis;
				}
				result[i][j]=rate;
			}
		}
		for(int i=0;i<vali.size();i++){
			double sum = 0;
			for(int j=0;j<6;j++) sum+=result[i][j];
			for(int j=0;j<6;j++) result[i][j]/=sum;
		}
		ofstream f(LIST+"vali_"+ss[K-3]+".csv",ios::trunc);
		for(int i=0;i<vali.size();i++){
			for(int j=0;j<6;j++) f<<result[i][j]<<",";
			f<<endl;
		}
		f.close();
	}
	
}
