#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <vector>
#include <algorithm>
#include <map>
#include <set>
using namespace std;

vector<int> train[101]; //训练集 
vector<int> test[101]; //测试集 
vector<int> train_labels, train_preds; //原始训练集标签和训练集预测标签 
vector<int> test_labels,  test_preds; //原始测试集标签和测试集预测标签 
int row1 = 0; //记录train集行数 
int row2 = 0;  //记录test集行数 
vector<int> w0; //初始权重 
vector<int> w ; //口袋算法中的全局权重向量 
int iteration = 0; //迭代次数 
int TP = 0, FN = 0, FP = 0, TN = 0;
double accuracy = 0, recall = 0, precision = 0, f1 = 0;
double eta = 1; //学习率 

bool null(string s){
	int len = s.length();
	for(int i=0; i<len; i++){
		if(s[i] != 32) return false;
	}
	return true;
}

int sign(int x){
	if(x>0)      return 1;
	else if(x<0) return -1;
	else         return 0;
} 

double Accuracy(int TP, int FN, int FP, int TN){ //正确率 
	return 1.0*(TP+TN) / (TP+FP+TN+FN);
}
double Recall(int TP, int FN){ //本来标签为+1的预测对的概率 
	return 1.0*TP / (TP+FN);
}
double Precision(int TP, int FP){ //预测结果为+1的预测对的概率 
	return 1.0*TP / (TP+FP);
}
double F1(double precision, double recall){
	return (2*precision*recall) / (precision+recall);
} 

int multiply(vector<int> a, vector<int> b)
{
	int sum = 0;
	int a_len = a.size(), b_len = b.size();
	if(a_len!=b_len) cout << "两个向量不一样长！\n";
	for(int i=0; i<a_len; i++){
		sum = sum + a[i]*b[i];
	}
	return sum;
}

int error(vector<int> w0) //计算当前w0值会分错多少个训练集中的样本 
{
	int error = 0, y = 0;
	for(int row=0; row<row1; row++)
	{
		y = sign(multiply(w0, train[row]));
		if(y != train_labels[row]){
			error ++;
		}
	}
	return error;
} 

int main()
{
	string str; //用来存每次getline进来的字符串 
	ifstream fin1, fin2, fin3 ,fin4;
	//cout << "请输入学习率（0~1）：\n"; 
	//cin >> eta;
//	int row1 = 0; //记录train集行数 
	fin1.open("train_data.txt");
	while(!fin1.eof()) 
	{
		train[row1].push_back(1); //先在每个x向量前补个1 
		getline(fin1, str, '\n');
		if(!null(str)){
			int pos, charact; //开始根据空格分词，charact存每个特征的取值 
			int size = str.size();
			string s, pattern = " ";
			for(int i=0; i<size; i++){
				pos = str.find(pattern, i);
				if(pos < size){
					s = str.substr(i, pos-i);
					sscanf(s.c_str(), "%d", &charact);
					train[row1].push_back(charact);
					/*if(row1 == 0){
						cout << charact << endl;
					}*/
				}
				i = pos + pattern.size()-1; 
			}	
			row1 ++;
		}
	}
	fin1.close();
	
//	int row2 = 0;  //记录test集行数 
	fin2.open("test_data.txt");
	while(!fin2.eof()) 
	{
		test[row2].push_back(1); //现在向量前补个1 
		getline(fin2, str, '\n');
		if(!null(str)){
			int pos, charact; //开始根据空格分词，charact存每个特征的取值 
			int size = str.size();
			string s, pattern = " ";
			for(int i=0; i<size; i++){
				pos = str.find(pattern, i);
				if(pos < size){
					s = str.substr(i, pos-i);
					sscanf(s.c_str(), "%d", &charact);
					test[row2].push_back(charact);
					/*if(row2 == 0){
						cout << charact << endl;
					}*/
				}
				i = pos + pattern.size()-1; 
			}	
			row2 ++;
		}
	}
	fin2.close();	
	
	fin3.open("train_labels.txt");
	int label;
	while(!fin3.eof())
	{
		getline(fin3, str, '\n'); //读取训练集标签 
		if(!null(str)){
			sscanf(str.c_str(), "%d", &label);
			train_labels.push_back(label);
		} 	
	}
	fin3.close();

	fin4.open("test_labels.txt");
	while(!fin4.eof())
	{
		getline(fin4, str, '\n'); //读取测试集标签 
		if(!null(str)){
			sscanf(str.c_str(), "%d", &label);
			test_labels.push_back(label);
		} 	
	}
	fin4.close();	
	
	//初始化权重
	for(int i=0; i<10001; i++){
		w0.push_back(0);
		w.push_back(0);
	} 
	
	for(int row=0; row<row1; row++)
	{
		int temp, y;
		temp = multiply(w0, train[row]);
		y = sign(temp);
		if(y != train_labels[row]){
			int len = train[row].size();
			for(int i=0; i<len; i++){ // 更新w，得 w = w + y1*x1 
				w0[i] = w0[i] + eta*train_labels[row]*train[row][i];
			}
			if(error(w0) < error(w)){
				w = w0;
			}
			row = -1; //重头开始检查 
			iteration ++;
		}
		
		if(iteration >= 1000) break; 
	} 
	
	for(int row=0; row<row2; row++)
	{
		int temp, y;
		temp = multiply(w, test[row]);
		y = sign(temp);
		test_preds.push_back(y);
	}
	
	for(int row=0; row<row2; row++)
	{
		if(test_labels[row]==1 && test_preds[row]==1)
			TP ++;
		else if(test_labels[row]==1 && test_preds[row]==-1)
			FN ++;
		else if(test_labels[row]==-1 && test_preds[row]==1)
			FP ++;
		else if(test_labels[row]==-1 && test_preds[row]==-1)
			TN ++;
	}
	
	accuracy = Accuracy(TP, FN, FP, TN);
	recall = Recall(TP, FN);
	precision = Precision(TP, FP);
	f1 = F1(precision, recall);
	int lenth = test[0].size();
	 
	cout << "测试集共有" << row2 << "行，" << lenth << "列：\n";
	cout << "迭代次数为："<< iteration << "次\n"; 
	cout << "此时各种计数器为：\n";
	cout << "TP = " << TP <<"; FN = " << FN << "; FP = " << FP << "; TN = " << TN << endl; 
	cout << "Accuracy is: " << accuracy << endl; 
	cout << "Recall is: " << recall << endl;
	cout << "Precision is: " << precision << endl;
	cout << "F1 is: " << f1 << endl;
	

} 













