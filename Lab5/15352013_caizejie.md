<center><font style=font-size:32px>**中山大学移动信息工程学院本科生实验报告** </font></center>

<center><font style=font-size:25px>**移动信息工程专业-人工智能** </font></center>

<center><font style=font-size:20px>**（2017-2018学年秋季学期）**</font></center>

<div><div style="float:left;">**课程名称:Artificial Intelligence**</div><div style="float:right">**任课老师:饶洋辉**</div><div style="clear: both;"></div></div>

------

| **教学班级** |     1501     | **专业（方向）** | 软件工程（互联网 |
| :------: | :----------: | :--------: | :------: |
|  **学号**  | **15352013** |   **姓名**   | **蔡泽杰**  |

------

### 一、实验题目

- 使用逻辑回归算法对数据进行分类。

### 二、实验内容

#### 1.算法原理

- 逻辑回归算法：模拟一个带权重的向量$W$来表示每一维度对结果产生的权重影响：
  $$
  S=\sum_{t=0}^dw_ix_i = w^Tx
  $$
  利用$logistic(sigmoid)$函数来将加权分数映射到一个合理的数据空间，使加权分数能够在$0-1$之间，因此，可以构造出一个新的函数模型：
  $$
  h(x) = \frac{1}{1+e^{-w^Tx}}
  $$
  通过求解得到的$w$带入后可以得到$p=h(x)$也就是概率值，通过判断概率的大小对数据进行分类。

  通过这种模型，可以得到似然函数$(likelihood)$：
  $$
  likelihood = \prod^M_{i=1}P(label|x_i) = \prod^M_{i=1} h(x_i)^{y_i}(1-h(x_i))^{1-y_i}
  $$
  当以上的似然函数达到最大时，模型参数$W$就能够很好的对数据集进行分类，也就是分类结果能够达到最大程度上的相似。那么，要对上式求得其最大值，则先将其取对数以达到求积变成求和的目的，然后通过添加负号将求最大值更改为求最小值：
  $$
  -log(likelihood)= -\sum_{i=1}^M [y_ilog(h(x_i)) + (1-y_i)log(1-h(x_i))]
  $$
  而想要求得上式的最小值，可以通过梯度下降法来不断迭代到逼近最低点：
  $$
  W_{k+1}=W_k-\alpha \frac{dW}{dw}
  $$
  那么就需要知道似然函数对应的求导公式：
  $$
  \begin{align}
  \frac{\partial -log(likelihood)}{\partial w_i} &= -\sum^N_{n=1} [(y_n)(\frac{\partial log(h(x_n))}{\partial h(x_n)})(\frac{\partial h(x_n)}{\partial w_i})+(1-y_n)(\frac{\partial log(1-h(x_n))}{\partial h(x_n)})(\frac{\partial h(x_n)}{\partial w_i})]
  \\ &= -\sum_{n=1}^N [(y_n)(\frac{1}{h(x_n)})-(1-y_n)(\frac{-1}{1-h(x_n)})][h(x_n)(1-h(x_n))](x_{n,i})
  \\ &= \sum _{n=1}^N(h(x_n)-y_n)(x_{n,i})
  \end{align}
  $$
  通过最终的迭代公式：
  $$
  W^{(j)}_{new} = W^{(j)}-\alpha \sum_{i=1}^n (\frac{1}{1+e^{-W^TX}}-y_i)X_i^{(j)}
  $$





#### 2.伪代码

- **RL算法:**

  ```c++
  while itera -- :迭代更新
  	for i=0:训练样本数
  		计算每个样本的权重分数wight;
  	for j=0:W的维数
  		for i=0:训练样本数
  			计算每个样本i对维度j的梯度贡献:[h(x)-y]*Xij;
  			求和sum;
  		更新Wj=Wj-步长*sum;
  ```

#### 3.关键代码

- **使用结构以及成员函数：**

  ```c++
  struct Matrix{   //存储矩阵X，以及每个样本对应额label 
  	vector <double> x;
  	double label;	//存储label值 +1或者0
  };
  void pretreat(string file,vector <Matrix> &set);//预处理 
  void divide(int k,vector <Matrix> &Data,vector <Matrix> &train,vector <Matrix> &val);//划分数据 
  void RL(vector <Matrix> &train,vector <double> &w_result);//梯度下降 
  double multi(Matrix &X,vector <double> &W);//向量乘法 
  void validation(vector <Matrix> &val,vector <double> &w_result);//计算准确率 
  ```

  ​


- **数据划分：**同样使用交叉验证的方法去准确率的平均值，所以将原始训练数据分成5份，划分的方式为取模，也就是通过数据下标对5取模，模值相同的为一组：

  ```c++
  void divide(int k,vector <Matrix> &Data,vector <Matrix> &train,vector <Matrix> &val){//划分数据
  	for(int i=0;i<Data.size();i++){ 
  		if(i%5==k) val.push_back(Data[i]); //根据属性值k来判断第几份数据作为验证集
  		else train.push_back(Data[i]); //剩余数据作为训练集
  	}
  } 
  ```

  然后在主函数中通过循环来达到交叉验证：

  ```c++
  for(int i=0;i<5;i++){ //将数据划分成五份，进行交叉验证 
    vector <Matrix> train; //训练集 
    vector <Matrix> val; //验证集
    divide(i,Data,train,val); //划分数据
    vector <double> w_result;
    RL(train,w_result); //进行梯度下降
    validation(val,w_result);//计算准确率 
  }
  ```

  最终将五份准确率求均值。

- **RL计算：** 根据公式进行计算：

  ```c++
  void RL(vector <Matrix> &train,vector <double> &w_result){
    //先初始化W
    for(int i=0;i<train[0].x.size();i++){w_result.push_back(1);} 
    double itera = 200;//迭代次数
    while(itera--){
      vector <double> weight;
      for(int i=0;i<train.size();i++){
        weight.push_back(multi(train[i],w_result));//计算每个样本的权重
      }
      //计算每一维梯度的下降：
      for(int j=0;j<w_result.size();j++){
        double sum = 0;
        for(int i=0;i<train.size();i++){
          //每一维梯度计算：
          sum+=(1/(1+exp(0-weight[i])) - train[i].label)*train[i].x[j]; 
        }
        w_result[j]-=(1/double(train.size()*10.0))*sum;//W更新，设定步长为样本数*10的倒数
      }
    }
  }
  ```

#### 4.创新点&优化

- 正则化：能够在一定程度上减少过拟合现象，在损失函数中惩罚较大项：
  $$
  -log(likelihood)= -\sum_{i=1}^M [y_ilog(h(x_i)) + (1-y_i)log(1-h(x_i))]+\lambda \sum_{j=1}W_j^2
  $$
  最后一项即为正则项，那么其梯度为：（实际上只是对添加项进行求导）
  $$
  \begin{align}
  \frac{\partial -log(likelihood)}{\partial w_j}=\sum _{n=1}^N(h(x_n)-y_n)(x_{n,i}) + 2\lambda W_j
  \end{align}
  $$
  其中，由于原有的$x_{n,0}$也就是第$0$维度的值是均定义为1的，也就不需要惩罚，所以上式只对$W_{j>0}$执行，惩罚值设定为$\lambda = 0.5*\alpha$.

- 动态学习率：为了提高迭代速率，采用动态学习率使迭代的步长能够随着迭代的次数增加而减小，为了取得一个平滑效果，假设初始的步长为$\alpha$，那么每次迭代的时候步长更新公式如下：
  $$
  \alpha = \alpha * 0.997
  $$
  如果$\alpha$的值设定为$2*(1/double(train.size()*10.0))$，可以得到步长与迭代次数的关系图：

  <center>![步长](E:\HomeWork\Grade_3_Last\AI\Lab5\步长.png)</center>

- 通过以上的定义可以得到更新公式代码：

  ```c++
  w_result[j]-=(1/double(train.size()*10.0))*(t)*(sum+w_result[j]);
  ```

  其中`t`即为动态步长变化值,每次迭代执行`t*=0.997;` 

### 三、实验结果及分析

#### 1.实验结果展示示例

- 实验数据：

  |      样本      |  特征1   |  特征2  |  标签   |
  | :----------: | :----: | :---: | :---: |
  |   $train1$   | **1**  | **1** | **0** |
  | **$train2$** | **1**  | **3** | **1** |
  | **$train3$** | **-2** | **1** | **1** |
  |  **$test$**  | **2**  | **3** | **?** |

  那么根据公式：
  $$
  S=\sum_{t=0}^dw_ix_i = w^Tx
  $$
  来计算权重：初始的$W$为$(1,1,1)$
  $$
  \begin{align}
  Weight_1 &= 1+1+1&=3\\
  Weight_2 &=1+1+3&=5\\
  Weight_3 &=1 - 2 + 1 &= 0
  \end{align}
  $$
  然后根据公式
  $$
  \nabla Cost=\alpha \sum_{i=1}^n (\frac{1}{1+e^{-W^TX}}-y_i)X_i^{(j)}
  $$
  计算各个维度的梯度：假设步长$\alpha$为1
  $$
  \begin{align}
  \nabla Cost(W_{0,0}) &= (\frac{1}{1+e^{-3}}-0)*1 +(\frac{1}{1+e^{-5}}-1)*1 +(\frac{1}{1+e^{0}}-1)*1 &=0.44588\\
  \nabla Cost(W_{0,1}) &= (\frac{1}{1+e^{-3}}-0)*1 +(\frac{1}{1+e^{-5}}-1)*1 + (\frac{1}{1+e^{0}}-1)*(-2)&=1.94588\\
  \nabla Cost(W_{0,2}) &= (\frac{1}{1+e^{-3}}-0)*1 +(\frac{1}{1+e^{-5}}-1)*3 + (\frac{1}{1+e^{0}}-1)*1&=0.43249\\
  \end{align}
  $$
  所以最终可以得到经过第一次迭代之后的$W$值为：
  $$
  \begin{align}
  W_1&=&[&(1-0.44588)&,&(1-1.94588)&,&(1-0.43249)&]&\\
  &=&[&0.55412&,&-0.94588&,&0.56751&]&
  \end{align}
  $$
  然后通过得到的$W_1$去预测测试样本，根据公式：
  $$
  h(x) = \frac{1}{1+e^{-w^Tx}}
  $$
  可以得到：
  $$
  p=\frac{1}{1+e^{-(0.55412*1-0.94588*2+0.56751*3)}}=0.59022 >0.5
  $$
  所以最终测试样本应该被判定为类别1：

  运行程序结果：

  <center><img src="E:\HomeWork\Grade_3_Last\AI\Lab5\运行结果.png" height = 200></center>

  显然运行结果与上面的计算结果一致，代码正确。


#### 2.评测指标展示及分析

- 通过迭代1000次，每次迭代输出交叉验证后得到的平均准确率，可以获得如下对比图：

  <center><img src="E:\HomeWork\Grade_3_Last\AI\Lab5\准确率数据.png" height = 280></center>

  显然进行正则化以及动态学习率优化之后，模型$W$能够更快收敛到最大似然值。

### 四、思考题

- **如果把梯度为 0 作为算法停止的条件，可能存在怎样的弊端？**

  可能存在无法终止迭代或者需要极大的迭代次数才能终止的弊端，因为其可能在梯度0附近不断地震荡而难以接近或无法接近0.

- $\eta$**的大小怎么影响梯度下降的结果？给出具体的解释，可视化的解释最好，比如图形展示等。**  

  |                   步长过小                   |                 步长适中（小）                  |
  | :--------------------------------------: | :--------------------------------------: |
  | <img src="E:\HomeWork\Grade_3_Last\AI\Lab5\步长小.jpg" height = 150> | <img src="E:\HomeWork\Grade_3_Last\AI\Lab5\步长中.jpg" height = 150> |
  |               **步长适中（大）**                |                 **步长过大**                 |
  | <img src="E:\HomeWork\Grade_3_Last\AI\Lab5\步长中_2.jpg" height = 150> | <img src="E:\HomeWork\Grade_3_Last\AI\Lab5\步长大.jpg" height = 150> |

  显然，步长的大小会影响每次梯度下降的大小，过小容易导致迭代到最优点的迭代次数大大增加，而过大则可能导致迭代不收敛，达不到最优点。所以需要选取适当的步长，才能够使得梯度下降得到一个合理的值，在保证收敛的情况下能够更快地迭代到最优点。

- **批梯度下降和随机梯度下降，思考这两种优化方法的优缺点。**

  - 批梯度下降：对于下降的梯度值，是综合了全部样本的结果，所以这种方式下降的梯度会使得最终的模型参数$W$能够更好地拟合整个样本集，但是相对地，由于需要计算全部样本，在数据量大的情况下，所需要的时间会大大增加。
  - 随机梯度下降：对于下降的梯度值，每次迭代的时候考虑某个随机样本的下降梯度值，经过多次迭代之后拟合整个样本，这种方式能够大大减少迭代运行时间，不过，可能需要进行更多次数的迭代才能够更好地拟合样本，得到一个合理的模型参数$W$，如果迭代次数不足够，容易导致最终参数$W$只是拟合了局部的样本，使得对测试集的预测不准确。