#include<iostream>
#include<vector>
#include<fstream>
#include<string>
#include<cmath>
#include<stdlib.h> 
#include<time.h>
using namespace std;

string PATH = "E:/HomeWork/Grade_3_Last/AI/Lab5/thurs7-8/";
struct Matrix{   //存储矩阵X，以及每个样本对应额label 
	vector <double> x;
	double label;	//存储label值 +1或者0
};

void pretreat(string file,vector <Matrix> &set);//预处理 
void divide(int k,vector <Matrix> &Data,vector <Matrix> &train,vector <Matrix> &val); //划分数据 
void RL(vector <Matrix> &train,vector <double> &w_result);//梯度下降 
double multi(Matrix &X,vector <double> &W);//向量乘法 
void validation(vector <Matrix> &val,vector <double> &w_result);//计算准确率

int main()
{	
	vector <Matrix> train; //训练集 
	vector <Matrix> val; //验证集
	pretreat("train.txt",train);
	pretreat("test.txt",val);
	vector <double> w_result;
	RL(train,w_result); //进行梯度下降
	
	validation(val,w_result);//计算准确率 

	return 0;
}

void pretreat(string file,vector<Matrix> &set){ //预处理 
	ifstream f(PATH+file);
	string data;
	while(getline(f,data)){
		Matrix X;
		X.x.push_back(1.0);
		int start=0,end=data.find(" ",start);
		while(end!=-1){
			string sub=data.substr(start,end-start);
			start=end+1;
			end=data.find(" ",start);
			double x=strtod(sub.c_str(),NULL);
			X.x.push_back(x);
		}
		X.label=(strtod(data.substr(start,data.size()-start).c_str(),NULL));
		set.push_back(X);
	}
}

void divide(int k,vector <Matrix> &Data,vector <Matrix> &train,vector <Matrix> &val){//划分数据
	for(int i=0;i<Data.size();i++){
		train.push_back(Data[i]);
	}
} 

double multi(Matrix &X,vector <double> &W){ //向量相乘 
	double m=0;
	for(int i=0;i<X.x.size();i++)
		m+=X.x[i]*W[i];
	return m;
}

void RL(vector <Matrix> &train,vector <double> &w_result){
	for(int i=0;i<train[0].x.size();i++){w_result.push_back(0);} 
	double itera = 1;//迭代次数
	while(itera--){
		vector <double> weight;
		cout<<"第 "<< 1-itera <<" 次迭代前W的值为：\n( ";
		for(int i=0;i<w_result.size();i++){
			cout<<w_result[i];
			if(i!=w_result.size()-1) cout<<" , ";
		}
		cout<<" )\n";
		cout<<"第 "<< 1-itera <<" 次迭代的样本权重如下：\n";
		for(int i=0;i<train.size();i++){
			weight.push_back(multi(train[i],w_result));//计算每个样本的权重 
			cout<<"train "<< i << ":  "<<weight[i]<<endl;
		}
		
		//计算每一维梯度的下降：
		cout<<"第 "<< 1-itera <<" 次迭代每一维度的下降值为：\n ";
		for(int j=0;j<w_result.size();j++){
			double sum = 0;
			for(int i=0;i<train.size();i++){
				sum+=(1/(1+exp(0-weight[i])) - train[i].label)*train[i].x[j]; 
			}
			//sum+=2*w_result[j]*w_result.size();
			cout<<sum<<" ";
			w_result[j]-=sum;//W更新 
		}
		cout<<endl;
		cout<<"第 "<< 1-itera <<" 次迭代后W的值为：\n( ";
		for(int i=0;i<w_result.size();i++){
			cout<<w_result[i];
			if(i!=w_result.size()-1) cout<<" , ";
		}
		cout<<" )\n\n";
	}
}

void validation(vector <Matrix> &val,vector <double> &w_result){//计算准确率 
	double l;
	for(int i=0;i<val.size();i++){
		double result = multi(val[i],w_result),label = -1;
		result = 1/(1+exp(0-result));
		cout<<"测试结果为："<<result<<endl;
		if(result-0.5>0){
			l = 1;
		} 
		else{
			l = 0;
		} 
		cout<<"测试样本 ：( ";
		for(int j=1;j<val[i].x.size();j++){
			cout<<val[i].x[j];
			if(j!=val[i].x.size()-1) cout<<" , ";
		}
		cout<<" ) 判定为："<<l<<endl;
	}
	
}
