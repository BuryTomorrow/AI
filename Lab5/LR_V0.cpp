#include<iostream>
#include<vector>
#include<fstream>
#include<string>
#include<cmath>
#include<stdlib.h> 
#include<time.h>
using namespace std;

string PATH = "E:/HomeWork/Grade_3_Last/AI/Lab5/";
struct Matrix{   //存储矩阵X，以及每个样本对应额label 
	vector <double> x;
	double label;	//存储label值 +1或者0
};

void pretreat(string file,vector <Matrix> &set);//预处理 
void divide(int k,vector <Matrix> &Data,vector <Matrix> &train,vector <Matrix> &val); //划分数据 
void RL(vector <Matrix> &train,vector <double> &w_result,vector <Matrix> &val,int k);//梯度下降 
double multi(Matrix &X,vector <double> &W);//向量乘法 
double validation(vector <Matrix> &val,vector <double> &w_result);//计算准确率 
void setResult();

double rate[5][1001];

int main()
{
	vector <Matrix> Data;
	vector <Matrix> test;
	pretreat("train.csv",Data);
	pretreat("text,csv",test);
	ofstream ff(PATH+"test.txt",ios::trunc);
	
	
	for(int i=0;i<5;i++){ //将数据划分成五份，进行交叉验证 
		vector <Matrix> train; //训练集 
		vector <Matrix> val; //验证集
		divide(i,Data,train,val); //划分数据
		
		vector <double> w_result;
		RL(train,w_result,val,i); //进行梯度下降
		
	}
//	int itera = 1000;
//	double t=1;
//	while(itera--){
//		double a = (1/double(6400.0*10.0))*(t);
//		t *= 0.997;
//		ff<<a<<endl;
//	}
	for(int i=1;i<999;i++){
		
		double sum = 0;
		for(int j=0;j<5;j++){
			sum+=rate[j][i];
		}
		ff<<sum/5<<endl;
		cout<<"迭代"<<i<<"次："<<sum/5<<endl;
	}
	return 0;
}

void pretreat(string file,vector<Matrix> &set){ //预处理 
	ifstream f(PATH+file);
	string data;
	while(getline(f,data)){
		Matrix X;
		X.x.push_back(1.0);
		int start=0,end=data.find(",",start);
		while(end!=-1){
			string sub=data.substr(start,end-start);
			start=end+1;
			end=data.find(",",start);
			double x=strtod(sub.c_str(),NULL);
			X.x.push_back(x);
		}
		X.label=(strtod(data.substr(start,data.size()-start).c_str(),NULL));
		set.push_back(X);
	}
}

void divide(int k,vector <Matrix> &Data,vector <Matrix> &train,vector <Matrix> &val){//划分数据
	for(int i=0;i<Data.size();i++){
		if(i%5==k) val.push_back(Data[i]);
		else train.push_back(Data[i]);
	}
} 

double multi(Matrix &X,vector <double> &W){ //向量相乘 
	double m=0;
	for(int i=0;i<X.x.size();i++)
		m+=X.x[i]*W[i];
	return m;
}

void RL(vector <Matrix> &train,vector <double> &w_result,vector <Matrix> &val,int k){
	for(int i=0;i<train[0].x.size();i++){w_result.push_back(1);} 
	int itera = 1000;//迭代次数
	int w=0;
	double t=2;
	while(itera--){
		w++;
		vector <double> weight;
		for(int i=0;i<train.size();i++){
			weight.push_back(multi(train[i],w_result));//计算每个样本的权重 
		}
		//计算每一维梯度的下降：
		for(int j=0;j<w_result.size();j++){
			double sum = 0;
			for(int i=0;i<train.size();i++){
				sum+=(1/(1+exp(0-weight[i])) - train[i].label)*train[i].x[j]; 
			}
			//sum+=2*w_result[j]*w_result.size();
			w_result[j]-=(1/double(train.size()*10.0))*(t)*(sum+w_result[j]);//W更新 
		}
		rate[k][w]=validation(val,w_result);
		t*=0.997;
	}
}

double validation(vector <Matrix> &val,vector <double> &w_result){//计算准确率 
	double correct = 0;
	for(int i=0;i<val.size();i++){
		double result = multi(val[i],w_result),label = -1;
		result = 1/(1+exp(0-result));
		if(result >0.5) label = 1;
		else label = 0;
		if(label == val[i].label) correct++;
	}
	correct = correct / double(val.size());
	//cout<<correct<<endl;
	return correct;
}

void setResult(){
	ofstream f(PATH+"15352013_caizejie.txt",ios::trunc);
}
