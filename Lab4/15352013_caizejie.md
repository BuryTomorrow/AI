<center><font style=font-size:32px>**中山大学移动信息工程学院本科生实验报告** </font></center>

<center><font style=font-size:25px>**移动信息工程专业-人工智能** </font></center>

<center><font style=font-size:20px>**（2017-2018学年秋季学期）**</font></center>

<div><div style="float:left;">**课程名称:Artificial Intelligence**</div><div style="float:right">**任课老师:饶洋辉**</div><div style="clear: both;"></div></div>

------

| **教学班级** |     1501     | **专业（方向）** | 软件工程（互联网 |
| :------: | :----------: | :--------: | :------: |
|  **学号**  | **15352013** |   **姓名**   | **蔡泽杰**  |

------

### 一、实验题目

- 实现决策树模型

### 二、实验内容

#### 1.算法原理

- **决策树：**一个属性结构的预测模型，代表特征与分类类别之间的一种映射关系。通过构建树形结构来预测数据的分类类别。由节点和有向边组成，其中节点分为内节点：一个特征值或特征属性  和  叶节点：分类类别。本质上是从训练集中通过概率计算归纳出一组分类规则，得到与数据集矛盾较小的决策树的同时具有很好的泛化能力。决策树的生成通常使用ID3，C4.5，CART三种概率模型算法来生成。

- **ID3：**表示已知某个特征值$X$而使得分类类别$Y$的不确定性减少的程度，定义为：
  $$
  g(Y,X)=H(Y)-H(Y|X)
  $$
  其中$H(Y)$是样本类别$Y$的经验熵，$H(Y|X)$为条件熵。

  - 经验熵$H(Y)$：也叫信息熵，通过熵的大小来表明结果的不确定性，熵越大，不确定性也就越大，由以下公式得到：
    $$
    H(Y)=-\sum^n_{i=1}p_ilog(p_i)
    $$

  - 条件熵$H(Y|X)$：表示已知$X$条件下随机变量$Y$的不确定性的大小，由以下公式得到：
    $$
    H(Y|X)=\sum^n_{i=1}p_iH(Y|X=x_i)
    $$
    其中$p_i=P(X=x_i)$，即条件$X$中$x_i$的概率。而$H(Y|X=x_i)$是条件$X$的属性值为$x_i$的情况下随机变量$Y$的不确定性。

- **C4.5：**信息增益比，与ID3算法相似，特征选择用的是信息增益率，也就是某个特征的信息增益与其自身的熵的比值，定义如下：
  $$
  g_R(Y,X)=\frac{H(Y)-H(Y|X)}{H(X)}
  $$
  这种方式能够在一定程度上避免因为特征中存在多个值而导致训练出一棵广度大而深度浅的树。

- **CART：**基尼指数，某个数据集的$gini$指数为： 
  $$
  gini(D)=\sum^n_{i=1}p_i(1-p_i)=1-\sum^n_{i=1}p_i^2
  $$
  同样的，可以通过$gini$指数的计算公式得到某特征条件$A$下的数据集$gini$指数：
  $$
  gini(D,A)=\sum^v_{j=1}p(A_j)*gini(D_j|A=A_j)
  $$
  其中$p(A_j)$即特征$A$中$A_j$发生的概率。

#### 2.伪代码

- **构建树：**

  ```c++
  Tree root;
  f(&root)递归构建树：
  	if 边界条件 == true ;结束递归
  	根据特征概率算法select特征;
  	根据所选特征创建子树Tree childs,根据特征属性分配子数据集D
  	for i=0:子树总量
  		f(&child);
  ```

  其中边界条件的判断为：

  ```c++
  if D为空集 or A为空集或D中所有样本在A中所有特征上取值相同 or D中样本都属于同一类别C
  	标记叶节点；
  	return true;
  return false;//不满足边界条件，继续递归
  ```

  其中标记叶节点的时候以多数原则判定分类类别。

  而选择特征则由如下流程完成：

  ```c++
  for i=0:数据集D中特征数量
  	计算特征概率算法，选择最佳属性，如ID3则选择信息增益最大的；
  ```

- **验证：**同样也是使用递归的方式自顶向下找到叶节点，然后预判类别：

  ```C++
  f(&root,data)
  	if 为叶节点:预判类别,结束递归
  	for i=0:子树childs总数量
  		find相同特征属性
  			f(&child,data);
  ```

#### 3.关键代码

- 首先需要将原始数据分为训练集和验证集：在这里使用交叉验证的方法，将原始的数据分为五份，方法为<code>i%5</code>：

  ```c++
  for(int i=0;i<train.size();i++){ //原始数据分为5份，一份作为验证集，其余作为训练集 
    if(i%5!=k)	root.add_data(train[i]);
    else val.push_back(train[i]);
  }
  ```

  该步骤实现在<code>init()</code>函数中：由主函数进行调用：

  ```c++
  for(int i=0;i<5;i++){ //交叉验证 
    Tree root; //树的根节点
    init(i,root);//初始化树，也就是将数据根据i值来分成五份进行交叉验证
    build(root);//构建树
    correct = 0;
    for(int j=0;j<val.size();j++){ //验证构建的树的准确率
      check(val[j],root);
    }
    double all = val.size();
    cout<<correct/all<<endl; 
  }
  ```

- 在构建树之前，需要先构建好树的结构：

  ```c++
  class Tree{		
  	public:
  		vector <Data> D; 		//结点下所具有的数据 
  		vector <Tree> child; 	//该结点下的子结点 
  		vector <int> selected; 	//维护某个特征是否已经选择过，没被选过的则为0
  		string feature;			// 记录该节点的具体特征属性 
  		string label; 			// 记录该结点的label属性是什么，对叶节点有效 
  		string father_label;	//记录父节点的众数label 
  		int  type;				//记录该结点是选哪个特征进行划分的，如果为叶节点则为 -1； 
  		
  		void add_data(Data &d); 	//添加数据 
  		void add_child(Tree &c); 	//添加子结点 
  		int select(vector <int> &f); 	//选择子结点特征,返回选择的第i个特征 
  		bool finish(); 		//判断是否作为叶节点，递归的边界条件 
  		void divide(int k); //根据所选的特征k划分数据集D 
  		void init_selected(vector <int> &sel);	//初始化维护结点是否被选过的数组
  		int getLength();	//返回特征数量 
  };
  ```

  其中数据结构Data以及全局变量，函数如下：

  ```C++
  struct Data{
  	vector <string> info;//记录文本属性 
  	string label;
  };
  vector<Data> train;	//训练集
  vector<Data> val;	//验证集 
  vector<Data> text;	//测试集 
  vector< vector<string> > infos;	//存储每个特征中具有哪些属性，方便对树进行分支

  void read(string file,vector<Data> &set);	//读取文件并存储
  void init(int k,Tree &root);//初始化数据 
  void build(Tree &t); //构建树
  //计算公式：传入的是数据集D的分类统计以及某个特征A下各个特征属性的分类统计：
  double ID3(double entropy_T,double entropy_F,vector<double> &cond_T,vector<double> &cond_F);
  double C4_5(double entropy_T,double entropy_F,vector<double> &cond_T,vector<double> &cond_F);
  double CART(double entropy_T,double entropy_F,vector<double> &cond_T,vector<double> &cond_F); 
  ```

- 最开始的<code>init()</code>函数做的工作是将读取的文本信息分割为训练集和验证集，并初始化树的内部数据，没什么好主意的，主要看构建树的过程：

  ```C++
  void build(Tree &t){ //构建树 
  	if(t.finish()) return; //如果已经到了叶节点，就结束递归 
  	
  	vector <int> group; 	//存储尚未使用的特征 
  	for(int i=0;i<t.getLength();i++){
  		if(t.selected[i]==0) group.push_back(i);
  	}
  	
  	t.divide(t.select(group));//筛选属性并进行划分划分数据
  	
  	for(int i=0;i<t.child.size();i++){//递归构建子树
  		build(t.child[i]);
  	}
  }
  ```

  树的构建过程其实是比较简单的，主要通过以下树结构中3个函数实现：

  - <code>finish()</code>：用于判断是否达到了边界条件，而边界条件则分为三个：

    - 判断数据集D是否为空集：因为分支是通过原有的初始数据集来进行划分的，所以子数据集中可能存在没有某个特征属性的分支，也就是说这个分支的数据集D为空，这个时候判断的类别则为父节点中出现最多的类别：

      ```c++
      if(D.size()==0){ //如果D为空集，则将当前节点标记为叶节点，类别为父节点中出现最多的类。 
        label = father_label;	//创建子树的时候都会给子树记录其父节点的多数类别。
        type = -1;
        return true;
      }
      ```

    - 判断A是否为空集以及D中所有样本在A中所有特征上取值是否都相同：

      ```C++
      bool flag1 = true;//判断特征集A是否为空集 
      vector <int> t;
      for(int i=0;i<selected.size();i++){
        if(selected[i]==0){ //这个是每棵树都具有的属性，记录这个节点及以上有哪些特征被使用过
          flag1 = false;
          t.push_back(i); //存下所有没被使用过的特征A，方便第二步判断
        } 
      }
      bool flag2 = true; //特征集A是否D的取值都相同 
      for(int i=0;i<t.size();i++){//遍历所有没被使用过的特征
        string l=D[0].info[t[i]];	
        for(int j=1;j<D.size();j++){	//判断数据集D中该特征属性是否都一致
          if(D[j].info[t[i]]!=l) flag2 = false;
        }
      }
      if(flag1||flag2){//若A为空集，或A上所有特征值取值相同，则将当前节点标记为叶节点，类别为众数类别 
        int  T=0,F=0;
        for(int i=0;i<D.size();i++){
          if(D[i].label == "-1") F++;
          else T++;
        }
        if(T>F) label = "1";
        else label = "-1";
        type = -1;//标记为叶节点 
        return true;
      }
      ```

    - 最后判断数据集D中所有样本是否属于同一类别C：

      ```c++
      bool flag3 = true;//判断D中所有样本是否都属于同一类别 
      for(int i=1;i<D.size();i++){ //遍历数据集D中的数据
        if(D[i].label!=D[0].label) flag3 = false;
      } 
      if(flag3){
        label = D[0].label;
        type = -1;
        return true;
      }
      ```

  - <code>select(group)</code>从还没有被使用过的特征中选出最优分类的特征：

    ```c++
    for(int i=0;i<f.size();i++){ //其中f为传入的未使用过的特征集合
      double entropy_T=0,entropy_F=0;//记录数据D集合中的两个label数量 
      vector<double> cond_T;
      vector<double> cond_F;//记录条件下的各个label数量 
      int t=f[i];
      for(int j=0;j<D.size();j++){
        if(D[j].label=="-1") entropy_F++;
        else entropy_T++;
      }
      for(int j=0;j<infos[t].size();j++){ //计数各个属性的label分布 
        double T = 0,F = 0;
        for(int k=0;k<D.size();k++){
          if(D[k].info[t]==infos[t][j]){
            if(D[k].label == "-1") F++;
            else T++;
          }
        }
        cond_T.push_back(T);cond_F.push_back(F);
      }
      //根据公式计算出该某一特征i的特征概率：其中CART返回的是1-gini方便后序操作
      double temp = ID3(entropy_T,entropy_F,cond_T,cond_F);//或C4_5或CART
      if(temp>max){	//信息增益和信息增益率都是找最大的，所以让gini返回的是1-最小得到最大：
        max = temp;
        best = t;	//记录最佳特征所在的列，作为返回值。
      }
    }
    ```

  - <code>divide(i)</code>根据第i类属性对数据集D进行划分：从上一个函数中可以得到应该对哪一列的特征进行分支，然后该函数就负责对子树进行赋值等：

    ```c++
    this->type=k; //该树以第K列作为特征进行划分
    this->selected[k]++; //记录该特征已被使用，相当于删除该列特征
    string f_label;//计算得到父节点中众数label 
    int T=0,F=0;
    for(int i=0;i<D.size();i++){
      if(D[i].label == "-1") F++;
      else T++;
    }
    if(T>F) f_label = "1";
    else f_label = "-1";
    //对划分的属性构建子树 
    for(int i=0;i<infos[k].size();i++){ //根据原始数据集中该列特征的总特征属性数量进行划分分支：
      Tree temp;
      temp.father_label = f_label;	//父节点的众数标签 
      temp.feature = infos[k][i]; 	//分支特征的具体属性值 
      for(int j=0;j<D.size();j++)	//为子结点加入数据D' 
        if(D[j].info[k]==temp.feature)
          temp.add_data(D[j]);
      temp.init_selected(selected); //初始化子树：确保子树能够继承已被选择的特征
      add_child(temp);//添加子树
    }
    ```

- 验证：验证集或测试集的预测：

  ```c++
  void check(Data &a,Tree &t){ //递归找到叶节点
  	if(t.type==-1){//叶节点 
  		if(t.label==a.label) correct++;//这句话用于验证集计算正确率
  		return ;
  	}
  	for(int i=0;i<t.child.size();i++){  //根据子树对应的分类属性自顶向下查找匹配
  		if(a.info[t.type]==t.child[i].feature){
  			check(a,t.child[i]);//递归
  		}
  	}
  }
  ```

#### 4.创新点&优化

- **后剪枝：**这里使用的后剪枝的原则是课上所讲的融入模型复杂度，也就是根据公式：
  $$
  e_c=\frac{\sum_{l-1}^L(r(n_l)+\zeta(n_l))}{\sum_{l=1}^Lm(n_l)}
  $$
  计算剪枝前后两者的误分类率$e_c$，如果剪枝后的误分类率更低，就确认剪枝，否则就不剪该枝。主要对建成的树进行如下操作：

  ```c++
  pruning(&tree)递归，自顶向下遍历树
  	if 叶节点 return true//告诉上一个递归下个节点是叶节点
  	if 所有下层节点都是叶节点
  		计算剪去下层所有叶节点，使该父节点成为叶节点的误分类率
  		if better 则剪枝 return true//告诉上一个递归该节点变成叶节点了
  return false//告诉上一个节点该节点没被剪枝同时不是叶节点
  ```

  具体代码函数<code>bool pruning(Tree &root)</code> ： 

  首先是函数一开始结束递归的条件以及判断有没有剪枝的资格：

  ```c++
  if(root.type == -1) return true;//达到叶节点时结束递归
  bool flag = true;//用于判断下一层节点是否全是叶节点 
  for(int i=0;i<root.child.size();i++){
    if(!pruning(root.child[i])){ //递归查看下层节点
      flag = false;//也就是下层节点中存在不剪枝的节点的话，那么该节点就没必要剪枝
    }
  }
  ```

  然后就是判断某个节点是否应该进行剪枝：这是在上面的<code>flag == true </code>的条件下才执行：

  ```c++
  double e1 = 0,e2 = 0;
  for(int i=0;i<root.D.size();i++){ //计算父节点作为叶节点时候的误分类数
    if(root.D[i].label != root.label) e1++;
  }
  for(int i=0;i<root.child.size();i++){ //计算下层叶节点的误分类数，也就是原误分类数
    for(int j=0;j<root.child[i].D.size();j++){
      if(root.child[i].D[j].label!=root.child[i].label) e2++;
    }
  }
  double new_error = error - e2 + e1;
  e1 = ( new_error + (leaf - double(root.child.size()) + 1)*punish )/train_count;
  e2 = (error + leaf*punish)/train_count;
  if(e1<e2){ //如果父节点融入复杂度后的误分类率更低，就剪枝 
    root.type = -1;//变更为叶节点 
    error = new_error;//更新原误分类数
    leaf = leaf - root.child.size() + 1;
    root.child.clear();//删除子集
    return true;//返回true，确保能够递归地剪枝 
  }
  ```

  其中惩罚系数为<code>punish = 0.5</code>，为可变参数。

### 三、实验结果及分析

#### 1.实验结果展示示例

- 使用课上样例：

  <center><img src="E:\HomeWork\Grade_3_Last\AI\Lab4\样例.png" height = 300></center>

  通过理论的计算可知，对于$ID3$模型来说，最终各个属性的信息增益为：
  $$
  \begin{eqnarray*}
  g(D,A="age") &=& 0.246\\
  g(D,A="income") &=& 0.029\\
  g(D,A="student") &=& 0.151\\
  g(D,A="credit\_rating") &=& 0.048\\
  \end{eqnarray*}
  $$
  显然最佳属性为$age$属性，所以第一次会分出三个岔路，其特征属性分别为$<=30$，$31...40$，$>40$。其中对于属性$31...40$可以发现其分类属性都是$yes$：

  |    age    |   income   | student | credit_rating | buts_computer |
  | :-------: | :--------: | :-----: | :-----------: | :-----------: |
  | **31~40** |  **high**  | **no**  |   **fair**    |    **yes**    |
  | **31~40** |  **low**   | **yes** | **excellent** |    **yes**    |
  | **31~40** | **medium** | **no**  | **excellent** |    **yes**    |
  | **31~40** |  **high**  | **yes** |   **fair**    |    **yes**    |

  也就是说该节点可以作为最终的叶节点了。然后继续划分剩下的两个属性，同样通过计算：

  - $<=30$：

  | age<=30 |   income   | student | credit_rating | buts_computer |
  | :-----: | :--------: | :-----: | :-----------: | :-----------: |
  |         |  **high**  | **no**  |   **fair**    |    **no**     |
  |         |  **high**  | **no**  | **excellent** |    **no**     |
  |         | **medium** | **no**  |   **fair**    |    **no**     |
  |         |  **low**   | **yes** |   **fair**    |    **yes**    |
  |         | **medium** | **yes** | **excellent** |    **yes**    |

  计算这个表的信息增益为：
  $$
  \begin{eqnarray*}
  g(D,A="income") &=& 0.97095-0.4&=&0.57095\\
  g(D,A="student") &=& 0.97095-0&=&0.97095\\
  g(D,A="credit\_rating") &=& 0.97095-0.95097&=&0.01998\\
  \end{eqnarray*}
  $$
  显然，属性$student$的信息增益最大，同时继续往下划分的两个属性正好可以作为叶节点。

  - $>40$：

  | age>40 |   income   | student | credit_rating | buts_computer |
  | :----: | :--------: | :-----: | :-----------: | :-----------: |
  |        | **medium** | **no**  |   **fair**    |    **yes**    |
  |        |  **low**   | **yes** |   **fair**    |    **yes**    |
  |        |  **low**   | **yes** | **excellent** |    **no**     |
  |        | **medium** | **yes** |   **fair**    |    **yes**    |
  |        | **medium** | **no**  | **excellent** |    **no**     |

  计算这个表的信息增益：
  $$
  \begin{eqnarray*}
  g(D,A="income") &=& 0.97095-0.95097&=&0.01998\\
  g(D,A="student") &=& 0.97095-0.95097&=&0.01998\\
  g(D,A="credit\_rating") &=& 0.97095-0&=&0.97095
  \end{eqnarray*}
  $$
  显然属性$credit\_rating$属性的信息增益最大，且继续往下分的两个属性也正好作为叶节点。

  从上分析可得最终将有5个叶节点，根节点为第0列也就是"age"属性，然后还具有两个中间节点，其中$age<=30$的分支属性以$student$特征（也就是第2列）作为节点特征继续往下分，而$age>40$的分支属性以$credit\_rating$特征（也就是第3列）为节点特征继续往下分。

- 观察实际输出结果：

  <center><img src="E:\HomeWork\Grade_3_Last\AI\Lab4\结果1.png" height = 200></center>

  可以看到输出属性值符合要求，同时树的建立也是正确，最终共5个节点。

- 对于剩下的两种计算模型：C4.5和CART，与上相比只是更改了计算公式，没什么太大的区别，构建的树分别如下：

  |                   C4.5                   |                   CART                   |
  | :--------------------------------------: | :--------------------------------------: |
  | <img src="E:\HomeWork\Grade_3_Last\AI\Lab4\结果2.png" height = 200> | <img src="E:\HomeWork\Grade_3_Last\AI\Lab4\结果3.png" height = 200> |


#### 2.评测指标展示及分析

- 以下数据结果均将训练集分成$5$份进行交叉验证得到平均准确率（去掉最高和最低准确率）：(感觉数据量太少)

|   准确率    |     不剪枝      |  剪枝(惩罚0.5)   |   剪枝(惩罚1)    |
| :------: | :----------: | :----------: | :----------: |
| **ID3**  | **0.631339** | **0.652597** | **0.650447** |
| **C4.5** | **0.620737** | **0.635639** | **0.650461** |
| **CART** | **0.633462** | **0.652597** | **0.650447** |

- 以下节点数为$5$份数据进行交叉验证方式得到的五棵决策树的平均叶子结点数：

|  叶子节点数   |    不剪枝    | 剪枝(惩罚0.5) |  剪枝(惩罚1)  |
| :------: | :-------: | :-------: | :-------: |
| **ID3**  | **693.6** | **304.6** | **261.4** |
| **C4.5** | **647.2** |  **296**  |  **263**  |
| **CART** |  **694**  | **300.6** | **256.8** |

可以看到相比于不剪枝的模型，其模型的复杂度大大降低，也就是叶子节点数在极大程度上减少了。同时，其准确率也能够在一定程度上得到提高。

### 四、思考题

- **决策树有哪些避免过拟合的方法？**

  - 剪枝：能够控制模型的强度，提升模型的泛化性能，同时避免过拟合，可分为预剪枝和后剪枝
  - 随机森林：使用随机森林，在各个子决策树上其数据拟合都很好，同时在测试集上由子决策树进行投票表决的结果同意也能达到一个不错的效果。

- **C4.5相比于ID3的优点是什么？**

  避免连续特征所带来的信息增益大的情况，克服了用信息增益选择属性时偏向于选择取值多的属性的不足。

- **如何用决策树来判断特征的重要性？**

  越接近根节点的特征对数据的重要程度越高。因为特征越接近根节点，那么其分类结果更容易被区分开来，相对地，特征越接近叶节点，那么其对数据的重要程度就越低，其区分性能越差。