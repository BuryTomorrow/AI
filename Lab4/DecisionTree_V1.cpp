#include<iostream>
#include<vector>
#include<fstream>
#include<string>
#include<cmath>
#include<algorithm>
using namespace std;

string PATH = "E:/HomeWork/Grade_3_Last/AI/Lab4/";

struct Data{
	vector <string> info;//记录文本属性 
	string label;
};

class Tree{
			
	public:
		vector <Data> D; //结点下所具有的数据 
		vector <Tree> child; //该结点下的子结点 
		vector <int> selected; //维护某个结点是否已经选择过，没被选过的则为0
		string feature;// 记录该节点的具体特征属性 
		string label; // 记录该结点的label属性是什么，对叶节点有效 
		string father_label;//记录父节点的众数label 
		int  type;//记录该结点是选哪个特征进行划分的，如果为叶节点则为 -1； 
		
		void add_data(Data &d); //添加数据 
		void add_child(Tree &c); //添加子结点 
		int select(vector <int> &f); //选择子结点特征,返回选择的第i个特征 
		bool finish(); //判断是否作为叶节点，递归的边界条件 
		void divide(int k); //根据所选的特征k划分数据集D 
		void init_selected(vector <int> &sel);//初始化维护结点是否被选过的数组
		
		int getLength();//返回特征数量 
};

vector<Data> train;//训练集
vector<Data> val;//验证集 
vector<Data> text;//测试集 
vector< vector<string>> infos;//存储每个特征中具有哪些属性 

void read(string file,vector<Data> &set);//读取文件并存储
void init(int k,Tree &root);//初始化数据 
void build(Tree &t); //构建树 
//计算公式： 
double ID3(double entropy_T,double entropy_F,vector<double> &cond_T,vector<double> &cond_F);
double C4_5(double entropy_T,double entropy_F,vector<double> &cond_T,vector<double> &cond_F);
double CART(double entropy_T,double entropy_F,vector<double> &cond_T,vector<double> &cond_F); 

double correct = 0;
void check(Data &a,Tree &t){
	if(t.type==-1){//叶节点 
		if(t.label==a.label) correct++;
		return ;
	}
	for(int i=0;i<t.child.size();i++){
		if(a.info[t.type]==t.child[i].feature){
			check(a,t.child[i]);
		}
	}
}

int main(){
	read("lab4_Decision_Tree/train.csv",train);
	
	for(int i=0;i<5;i++){ //交叉验证 
		Tree root;
		init(i,root);
		build(root);
		correct = 0;
		for(int j=0;j<val.size();j++){
			check(val[j],root);
		}
		double all = val.size();
		cout<<correct/all<<endl; 
	}
	return 0;
}

double getlog(double p){ //返回log值 
	if(p==0) return 0;
	return (0-p*log(p)/log(2));
}

double ID3(double entropy_T,double entropy_F,vector<double> &cond_T,vector<double> &cond_F){
	double HD,HD_A=0,GD_A,sum=entropy_T+entropy_F;
	if(sum==0) return 0;
	double pt = entropy_T/sum,pf=entropy_F/sum;
	HD = getlog(pt) + getlog(pf);		//计算得到数据集D的经验熵 
	for(int i=0;i<cond_T.size();i++){ //计算条件熵 
		double sum2 = cond_T[i]+cond_F[i];
		if(sum2==0) continue;
		HD_A+=(sum2/sum)*(getlog(cond_T[i]/sum2)+getlog(cond_F[i]/sum2));
	}
	//信息增益 
	GD_A = HD - HD_A; 
	return GD_A; 
}

double C4_5(double entropy_T,double entropy_F,vector<double> &cond_T,vector<double> &cond_F){
	double HD,HD_A=0,GD_A,HA=0,sum=entropy_T+entropy_F;
	if(sum==0) return 0;
	double pt = entropy_T/sum,pf=entropy_F/sum;
	HD = getlog(pt) + getlog(pf);		//计算得到数据集D的经验熵 
	for(int i=0;i<cond_T.size();i++){ //计算条件熵 
		double sum2 = cond_T[i]+cond_F[i];
		if(sum2==0) continue;
		HD_A+=(sum2/sum)*(getlog(cond_T[i]/sum2)+getlog(cond_F[i]/sum2));
	}
	//信息增益 
	GD_A = HD - HD_A; 
	//计算A的熵
	for(int i=0;i<cond_T.size();i++){
		double sum2 = cond_T[i]+cond_F[i];
		HA+=getlog(sum2/sum);
	}
	//信息增益率：
	GD_A /= HA;
	return GD_A; 
} 

double CART(double entropy_T,double entropy_F,vector<double> &cond_T,vector<double> &cond_F){
	double sum = entropy_T+entropy_F,gini=0;
	if(sum == 0) return 0;
	for(int i=0;i<cond_T.size();i++){
		double sum2=cond_T[i]+cond_F[i];
		if(sum2==0) continue;
		gini+=sum2/sum*(1-(cond_T[i]/sum2)*(cond_T[i]/sum2)-(cond_F[i]/sum2)*(cond_F[i]/sum2));
	}
	return 1.0-gini;
} 

void build(Tree &t){ //构建树 
	if(t.finish()) return; //如果已经到了叶节点，就结束递归 
	
	vector <int> group; //存储尚未使用的特征 
	for(int i=0;i<t.getLength();i++){
		if(t.selected[i]==0) group.push_back(i);
	}
	
	t.divide(t.select(group));//筛选属性并进行划分划分数据
	
	for(int i=0;i<t.child.size();i++){//递归构建子树
		build(t.child[i]);
	}
}

void init(int k,Tree &root){//初始化数据
	val.clear();
	vector <int> selected;
	for(int i=0;i<train[0].info.size();i++){
		selected.push_back(0);
	}
	root.init_selected(selected);
	root.type = 0;
	for(int i=0;i<train.size();i++){ //原始数据分为5份，一份作为验证集，其余作为训练集 
		if(i%5!=k)	root.add_data(train[i]);
		else val.push_back(train[i]);
	}
	
	//初始化infos，存储每个特征中的各类属性
	infos.clear();
	for(int j=0;j<train[0].info.size();j++){
		vector <string> temp;
		for(int i=0;i<train.size();i++){
			if(i%5==k) continue;//验证集不参与计算 
			if(find(temp.begin(),temp.end(),train[i].info[j])==temp.end())
				temp.push_back(train[i].info[j]);
		}
		infos.push_back(temp);
	}
}

void read(string file,vector<Data> &set){	//读取文件并存储 
	ifstream f(PATH+file);
	string data;
	while(getline(f,data)){
		Data d;
		int start=0,end=data.find(",",start);
		while(end!=-1){
			string sub=data.substr(start,end-start);
			start=end+1;
			end=data.find(",",start);
			d.info.push_back(sub);
		}
		d.label=(data.substr(start,data.size()-start));
		set.push_back(d);
	}
}

/////////////////////////Tree的实现函数///////////////////////////////
void Tree::add_data(Data &d){//添加数据
	D.push_back(d);
}
void Tree::add_child(Tree &c){//添加子结点 
	child.push_back(c);
} 
int Tree::select(vector <int> &f){//选择子结点特征,返回选择的第i个特征
	
	double max = 0;
	int best = -1;
	
	for(int i=0;i<f.size();i++){
		double entropy_T=0,entropy_F=0;//记录数据D集合中的两个label数量 
		vector<double> cond_T;
		vector<double> cond_F;//记录条件下的各个label数量 
		int t=f[i];
		for(int j=0;j<D.size();j++){
			if(D[j].label=="-1") entropy_F++;
			else entropy_T++;
		}
		for(int j=0;j<infos[t].size();j++){ //计数各个属性的label分布 
			double T = 0,F = 0;
			for(int k=0;k<D.size();k++){
				if(D[k].info[t]==infos[t][j]){
					if(D[k].label == "-1") F++;
					else T++;
				}
			}
			cond_T.push_back(T);
			cond_F.push_back(F);
		}
		
		double temp = ID3(entropy_T,entropy_F,cond_T,cond_F);
		
		if(temp>max){
			max = temp;
			best = t;
		}
	}
	return best;
}  

bool Tree::finish(){//判断是否作为叶节点，递归的边界条件
	if(D.size()==0){ //如果D为空集，则将当前节点标记为叶节点，类别为父节点中出现最多的类。 
		label = father_label;
		type = -1;
		return true;
	}
	
	bool flag1 = true;//判断特征集A是否为空集 
	vector <int> t;
	for(int i=0;i<selected.size();i++){
		if(selected[i]==0){
			flag1 = false;
			t.push_back(i);
		} 
	}
	
	bool flag2 = true; //特征集A是否D的取值都相同 
	for(int i=0;i<t.size();i++){
		string l=D[0].info[t[i]];
		for(int j=1;j<D.size();j++){
			if(D[j].info[t[i]]!=l) flag2 = false;
		}
	}
	if(flag1||flag2){//若A为空集，或A上所有特征值取值相同，则将当前节点标记为叶节点，类别为众数类别 
		int  T=0,F=0;
		for(int i=0;i<D.size();i++){
			if(D[i].label == "-1") F++;
			else T++;
		}
		if(T>F) label = "1";
		else label = "-1";
		type = -1;//标记为叶节点 
		return true;
	}
	
	bool flag3 = true;//判断D中所有样本是否都属于同一类别 
	for(int i=1;i<D.size();i++){
		if(D[i].label!=D[0].label) flag3 = false;
	} 
	if(flag3){
		label = D[0].label;
		type = -1;
		return true;
	}
	return false;
}  

void Tree::divide(int k){//根据所选的特征k划分数据集D
	this->type=k;
	this->selected[k]++;
	//计算父节点中众数label 
	string f_label;
	int T=0,F=0;
	for(int i=0;i<D.size();i++){
		if(D[i].label == "-1") F++;
		else T++;
	}
	if(T>F) f_label = "1";
	else f_label = "-1";
	
	//对划分的属性构建子树 
	for(int i=0;i<infos[k].size();i++){
		Tree temp;
		temp.father_label = f_label;//父节点的众数标签 
		temp.feature = infos[k][i]; //分支特征的具体属性值 
		//为子结点加入数据D' 
		for(int j=0;j<D.size();j++){
			if(D[j].info[k]==temp.feature){
				temp.add_data(D[j]);
			}
		}
		temp.init_selected(selected);
		
		add_child(temp);
	}
	
}

void Tree::init_selected(vector <int> &sel){//初始化维护结点是否被选过的数组 
	for(int i=0;i<sel.size();i++)
		selected.push_back(sel[i]);
}

int Tree::getLength(){//返回特征数量
	if(D.size()==0) return 0;
	return D[0].info.size();
}
