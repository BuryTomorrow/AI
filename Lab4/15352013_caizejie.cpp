#include<iostream>
#include<vector>
#include<fstream>
#include<string>
#include<cmath>
using namespace std;

string PATH = "E:/HomeWork/Grade_3_Last/AI/Lab4/";

struct Data{
	vector <string> info;//记录文本属性 
	string label;
};
struct Feature{
	string type;
	double count;
	double p_T;
	double p_F;
};
vector<Data> train;
vector<Data> text;

void read(string file,vector<Data> &set); 

void getInfoGain();

int main(){
	read("lab4_Decision_Tree/homework.csv",train);
	getInfoGain();
//	for(int i=0;i<train.size();i++){
//		for(int j=0;j<train[i].info.size();j++){
//			cout<<train[i].info[j]<<" ";
//		}
//		cout<<train[i].label<<endl;
//	}
	return 0;
}

void read(string file,vector<Data> &set){
	ifstream f(PATH+file);
	string data;
	while(getline(f,data)){
		Data d;
		int start=0,end=data.find(",",start);
		while(end!=-1){
			string sub=data.substr(start,end-start);
			start=end+1;
			end=data.find(",",start);
			d.info.push_back(sub);
		}
		d.label=(data.substr(start,data.size()-start));
		set.push_back(d);
	}
}

void getInfoGain(){
	double p_T=0,p_F=0;
	for(int i=0;i<train.size();i++){
		if(train[i].label=="1") p_T++;
		else p_F++;
	}
	p_T=p_T/(double(train.size()));
	p_F=p_F/(double(train.size()));
	double H = 0-p_T*(log(p_T)/log(2.0))-p_F*(log(p_F)/log(2.0));//熵 
	cout<<H<<endl; 
	vector<double> Hc;//各列的条件熵 
	vector<double> Hf;//各列的熵 
	for(int j=0;j<train[0].info.size();j++){ //各列特征值 
		vector <Feature> f;
		for(int i=0;i<train.size();i++){  //计数每种属性数量以及label分类数。 
			int k;
			for(k=0;k<f.size();k++){
				if(f[k].type==train[i].info[j]){
					f[k].count++;
					if(train[i].label == "1"){
						f[k].p_T++;
					}else{
						f[k].p_F++;
					}
					break;
				}
			}
			if(k==f.size()){
				Feature temp;
				temp.type = train[i].info[j];
				temp.count = 1.0;
				if(train[i].label == "1"){
					temp.p_T=1;
					temp.p_F=0;
				}else{
					temp.p_T=0;
					temp.p_F=1;
				}
				f.push_back(temp);
			}
		}
		//计算条件熵：
		double hc = 0;
		for(int i=0;i<f.size();i++){
			double p1=f[i].count/double(train.size());
			double T = f[i].p_T/f[i].count;
			double F = f[i].p_F/f[i].count;
			double pT,pF;
			if(T==0){
				pT=0;
			}else{
				pT=0-T*log(T)/log(2.0);
			}
			if(F==0){
				pF=0;
			}else{
				pF=0-F*log(F)/log(2.0);
			}
			hc+=p1*(pT+pF);
			
		}
		Hc.push_back(hc);
		//计算熵
		double hf=0;
		for(int i=0;i<f.size();i++){
			double p=f[i].count/(double(train.size()));
			hf+=0-p*log(p)/log(2.0);
		}
		Hf.push_back(hf);
	}
	//计算信息增益率：
	double max = 0;
	int k=0;
	for(int i=0;i<train[0].info.size();i++){
		if((H-Hc[i])/Hf[i]>max){
			max=(H-Hc[i])/Hf[i];
			k=i;
		}
		cout<<"第 "<<i<<" 列属性：\n";
		cout<<"信息增益为："<<H-Hc[i]<<endl;
		cout<<"信息熵为: "<<Hf[i]<<endl;
		cout<<"信息增益率为: "<<(H-Hc[i])/Hf[i]<<endl<<endl;
	}
	cout<<"最佳的属性为第 "<<k<<" 列。信息增益率为："<<max<<endl;
}
