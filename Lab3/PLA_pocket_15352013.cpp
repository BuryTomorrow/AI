#include<iostream>
#include<vector>
#include<fstream>
#include<string>
#include<cmath>
#include<stdlib.h> 
#include<time.h>
using namespace std;

string PATH = "E:/HomeWork/Grade_3_Last/AI/Lab3/lab3数据/";
struct Matrix{   //存储矩阵X，以及每个样本对应额label 
	vector <double> x;
	double label;	//存储label值 +1或者-1，对于w则是存储最优值 
};

vector <Matrix> train;
vector <Matrix> val;
vector <Matrix> test;
Matrix w_result;
Matrix best_accuracy;
Matrix best_precision;
Matrix best_recall;
Matrix best_F1;
/* 
TP:本来为+1，预测为+1
FN:本来为+1，预测为-1
TN:本来为-1，预测为-1
FP:本来为-1，预测为+1 
accuracy:	准确率：(TP+TN)/ALL 
precision:	精确率: (TP)/(TP+FP)
recall:		召回率: (TP)/(TP+FN)
f1:			F值: 	
*/
double TP,FN,TN,FP;


void pretreat(string file,vector <Matrix> &set);
void PLA_pocket();
void findbest();
double multi(Matrix X,Matrix W);
void setResult();

void check(Matrix best){
	TP=0;FN=0;TN=0;FP=0;
	for(int i=0;i<val.size();i++){
		if(val[i].label==1){
			if(multi(val[i],best)==1) TP++;
			else FN++;
		}else{
			if(multi(val[i],best)==-1) TN++;
			else FP++;
		}
	}
	double accuracy=(TP+TN)/(TP+FP+TN+FN),precision=(TP)/(TP+FP),recall=TP/(TP+FN);
	double F1=2*precision*recall/(precision+recall);
	cout<<"accuracy : "<<accuracy<<endl;
	cout<<"precision : "<<precision<<endl;
	cout<<"recall : "<<recall<<endl;
	cout<<"F1 : "<<F1<<endl;
}

int main()
{
	pretreat("train.csv",train);
	pretreat("val.csv",val);
	pretreat("test.csv",test);
	PLA_pocket();
	setResult();
	
	cout<<"以accuracy为最优选择 ：\n";
	check(best_accuracy);
	cout<<"\n以precision为最优选择 ：\n";
	check(best_precision);
	cout<<"\n以recall为最优选择 ：\n";
	check(best_recall);
	cout<<"\n以F1为最优选择 ：\n";
	check(best_F1); 
	return 0;
}

void pretreat(string file,vector<Matrix> &set){ //预处理 
	ifstream f(PATH+file);
	string data;
	while(getline(f,data)){
		Matrix X;
		X.x.push_back(1.0);
		int start=0,end=data.find(",",start);
		while(end!=-1){
			string sub=data.substr(start,end-start);
			start=end+1;
			end=data.find(",",start);
			double x=strtod(sub.c_str(),NULL);
			X.x.push_back(x);
		}
		X.label=(strtod(data.substr(start,data.size()-start).c_str(),NULL));
		set.push_back(X);
	}
}

double multi(Matrix X,Matrix W){ //向量相乘 
	double m=0;
	for(int i=0;i<X.x.size();i++)
		m+=X.x[i]*W.x[i];
	if(m>0) return 1.0;
	if(m<0)	return -1.0;
	return 0;
}

void PLA_pocket(){
	best_accuracy.label=0;best_precision.label=0;best_recall.label=0;best_F1.label=0;
	for(int i=0;i<train[0].x.size();i++){
		w_result.x.push_back(0);
		best_accuracy.x.push_back(0);
		best_precision.x.push_back(0);
		best_recall.x.push_back(0);
		best_F1.x.push_back(0);
	}
	int itera = 4000;//迭代次数
	while(itera--){
		srand(itera);
		for(int i=rand()%3000;i<train.size();i++){
			if(multi(train[i],w_result)==train[i].label) continue; 	//如果预测正确则找下一个样本 
			for(int j=0;j<w_result.x.size();j++)	//预测错误则更新 
				w_result.x[j]+=train[i].label*train[i].x[j];
			findbest(); //更新最优解
			break; 
		}
	}
}

void Update(double value,Matrix &best){
	if(value>best.label){
		for(int i=0;i<best.x.size();i++){
			best.x[i]=w_result.x[i];
		}
		best.label=value; 
	}
}

void findbest(){
	TP=0;FN=0;TN=0;FP=0;
	for(int i=0;i<train.size();i++){
		if(train[i].label==1){
			if(multi(train[i],w_result)==1) TP++;
			else FN++;
		}else{
			if(multi(train[i],w_result)==-1) TN++;
			else FP++;
		}
	}
	double accuracy=(TP+TN)/(TP+FP+TN+FN),precision=(TP)/(TP+FP),recall=TP/(TP+FN);
	double F1=2*precision*recall/(precision+recall);
	Update(accuracy,best_accuracy);
	Update(precision,best_precision);
	Update(recall,best_recall);
	Update(F1,best_F1);
}

void setResult(){
	ofstream f(PATH+"15352013_caizejie_PLA.csv",ios::trunc);
	for(int i=0;i<test.size();i++){
		double out = multi(best_accuracy,test[i]);
		f<<out<<endl;
	}
}
