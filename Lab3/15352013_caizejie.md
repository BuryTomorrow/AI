<center><font style=font-size:32px>**中山大学移动信息工程学院本科生实验报告** </font></center>

<center><font style=font-size:25px>**移动信息工程专业-人工智能** </font></center>

<center><font style=font-size:20px>**（2017-2018学年秋季学期）**</font></center>

<div><div style="float:left;">**课程名称:Artificial Intelligence**</div><div style="float:right">**任课老师:饶洋辉**</div><div style="clear: both;"></div></div>

------

| **教学班级** |     1501     | **专业（方向）** | 软件工程（互联网 |
| :------: | :----------: | :--------: | :------: |
|  **学号**  | **15352013** |   **姓名**   | **蔡泽杰**  |

------

### 一、实验题目

- 感知机算法：使用PLA算法对二分数据进行分类。

### 二、实验内容

#### 1.算法原理

- 原始算法和口袋算法原理是一致的，只不过口袋算法相比于原始算法，则是选取最优解而不是选取最后迭代得到的结果。具体原理如下：

  假设所有数据都具备向量特征：
  $$
  X_i=(1,x_1,x_2,x_3,...,x_d);
  $$
  同时所有的数据都具有各自$label$，仅分为两类，以$+1$和$-1$两个符号表示，即：
  $$
  Y_i={(-1)}or{(+1)}
  $$
  接下来假设存在分割面$W$能够完全分割两种类型的数据，那么就会得到如下效果
  $$
  all[Y_i=sign(W^T_iX_i^{'} )]
  $$
  那么，想要找到这个分割面，就需要进行迭代运算：

  当找到一个不匹配的预测值：
  $$
  Y_i\ne sign(W^T_iX_i^{'} )
  $$
  则更新分割面：
  $$
  W_{t+1}=W_t+(y_i*X_i)
  $$
  最终可以得到最后的分割面。不过，想要实现完全的分割，数据就必须要求存在线性面能够完全分割，但实际数据中基本上不存在这样的分割面，所以我们需要设定一定的阈值（在本次实验中使用迭代次数）来让迭代停止。因此，得到的分割效果必然不是完全分割，为了衡量分割效果的好坏程度，引入四个指标：Accuracy(准确率)，Precision(精确率)，Recall(召回率)，F1(F值)。其中准确率为全部数据中预测正确预测的占比，精确率为预测结果为$+1$的数据中预测正确的占比，召回率为数据真实$label$为$+1$的情况下预测正确的占比，而F值则作为衡量精确率和召回率的效果好坏。

#### 2.伪代码

- 原始算法：比较简单，只需要设定好迭代次数，不断对文本进行遍历计算即可。

  ```c++
  初始W
  X[]->{1,X[]}//给所有样本添加x0=1
  while(itera--)
    for i=0:训练文本数
    	if 计算label与真实label不同
    		更新W，进入下一次迭代
  ```

- 口袋算法：和原始算法相似，不过需要保留最优解

  ```C++
  初始W,best_W
  X[]->{1,X[]}//给所有样本添加x0=1
  while(itera--)
    for i=0:训练文本数
    	if 计算label与真实label不同
    		更新W
    		if 新W对于训练文本相比best_W错误率更低，更新best_W
    		进入下一次迭代
  ```

#### 3.关键代码

- 预处理：只是读取文本的同时给所有的样本都加上一个$x_0$，方便后面计算，以及分割出最后的$label$并将其从字符串转为$double$类型方便计算即可。不做重点。

- 原始算法函数：<code>void PLA_initial()</code>

  ```C++
  void PLA_initial(){
  	for(int i=0;i<train[0].x.size();i++) w_result.x.push_back(0);//初始化W0 
  	int itera = 9999;//迭代次数
  	while(itera--){
  		for(int i=0;i<train.size();i++){
  			if(multi(train[i],w_result)==train[i].label) continue; 	//如果预测正确则找下一个样本 
  			for(int j=0;j<w_result.x.size();j++){	//预测错误则更新 
  				w_result.x[j]+=train[i].label*train[i].x[j];
  			}
  			break; //更新完就跳到下一次迭代
  		}
  	}
  }
  ```

  其中编写向量乘法函数<code>multi()</code>:

  ```C++
  double multi(Matrix X,Matrix W){
  	double m=0;
  	for(int i=0;i<X.x.size();i++)//向量乘法
  		m+=X.x[i]*W.x[i];
  	if(m>0) return 1.0;//sign判断
  	if(m<0)	return -1.0;
  	return 0;//如果两类数据都不是，返回一个0表示不属于任何类型，即预测错误
  }
  ```

- 口袋算法：代码类似：

  ```C++
  void PLA_pocket(){
  	best_accuracy.label=0;best_precision.label=0;best_recall.label=0;best_F1.label=0;
  	//初始化W0以及四个指标作为标准下各个最优解
    	for(int i=0;i<train[0].x.size();i++){
  		w_result.x.push_back(0);
  		best_accuracy.x.push_back(0);
  		best_precision.x.push_back(0);
  		best_recall.x.push_back(0);
  		best_F1.x.push_back(0);
  	}
  	int itera = 4000;//迭代次数
  	while(itera--){
  		for(int i=0;i<train.size();i++){
  			if(multi(train[i],w_result)==train[i].label) continue; 	//如果预测正确则找下一个样本 
  			for(int j=0;j<w_result.x.size();j++)	//预测错误则更新 
  				w_result.x[j]+=train[i].label*train[i].x[j];
            	findbest(); //更新最优解
  			break; 
  		}
  	}
  }
  ```

  其中更新最优解函数<code>findbest</code>的代码则是通过计算使用更新后的<code>w_result</code>值来测试训练集，获取训练集下的四个指标值，对比原有的最优解，如果更优则更新最优解，更新判断主要由以下函数负责：

  ```C++
  void Update(double value,Matrix &best){
  	if(value>best.label){//判断新的W的指标是否更高，更高则更新
  		for(int i=0;i<best.x.size();i++){//更新最优解
  			best.x[i]=w_result.x[i];
  		}
  		best.label=value; //更新最优指标
  	}
  }
  ```

#### 4.创新点&优化

- 本次实验的优化主要在于口袋算法的迭代方式。一般来说，每次迭代都是从第1个样本开始往后遍历所有样本来找出预测错误的样本，所以，优化的方法可以是从训练样本中随机一个位置开始往后查找，从而尝试提高指标：（当然也可以设定初始的W值）

  ```c++
  int itera = 4000;//迭代次数
  	while(itera--){
  		srand(itera);//设定随机数种子
  		for(int i=rand()%3000;i<train.size();i++){
  			if(multi(train[i],w_result)==train[i].label) continue; 	//如果预测正确则找下一个样本 
  			for(int j=0;j<w_result.x.size();j++)	//预测错误则更新 
  				w_result.x[j]+=train[i].label*train[i].x[j];
  			findbest(); //更新最优解
  			break; 
  		}
  	}
  ```


### 三、实验结果及分析

#### 1.实验结果展示示例

- 编写小数据集如下：

  |     样本     | $x_1$ | $x_2$ |  label   |
  | :--------: | :---: | :---: | :------: |
  | **train1** | **1** | **1** | **$-1$** |
  | **train2** | **2** | **2** | **$+1$** |
  | **train3** | **2** | **3** | **$-1$** |
  | **train4** | **3** | **1** | **$+1$** |

- 初始W为$(0,0,0)$进行迭代：
  $$
  W_0(0.0.0)*train_1\to W_1(-1,-1,-1)\to W_1*train_2\to W_2(0,1,1)\to ....\to W_{39}(-5,5,2)
  $$
  因为迭代次数较多久不一一计算了。

- 理论分析下可以画出如下分界线：

  <center><img src="E:\HomeWork\Grade_3_Last\AI\Lab3\分割图.jpg" height = 300></center>

  最终进行运行后得到的结果为：

  |                  计算过程1                   |                  计算过程2                   |                   最终结果                   |
  | :--------------------------------------: | :--------------------------------------: | :--------------------------------------: |
  | <img src="E:\HomeWork\Grade_3_Last\AI\Lab3\实际结果过程1.jpg" height = 220> | <img src="E:\HomeWork\Grade_3_Last\AI\Lab3\实际结果过程2.jpg" height = 220> | <img src="E:\HomeWork\Grade_3_Last\AI\Lab3\实际结果图.jpg" height = 170> |

  可以看到计算过程中前面的的W1，W2等都与理论计算相同，计算下去也是同样的结果，最终的W值为$(-5 , 5 , -2)$，根据公式可以得到分割线函数为：
  $$
  -5+5x-2y=0
  $$
  与图中结果相吻合，正确分割两类数据。


#### 2.评测指标展示及分析

- 验证集结果对比：

|                   原始算法                   |                 口袋算法无优化                  |                  口袋算法优化                  |
| :--------------------------------------: | :--------------------------------------: | :--------------------------------------: |
| <img src="E:\HomeWork\Grade_3_Last\AI\Lab3\原始算法.jpg " width = 200> | <img src="E:\HomeWork\Grade_3_Last\AI\Lab3\口袋无优化.jpg" height = 220> | <img src="E:\HomeWork\Grade_3_Last\AI\Lab3\口袋优化.jpg" height = 220> |

从三个算法得到的W应用于验证集可以看出，经过优化的口袋算法效果最优，同时，以accuracy作为最优选择标准得到的四个指标效果最优。在口袋算法无优化的情况下，可以看到如果以accuracy作为衡量最优标准，会出现nan值，原因是训练集中$-1$的数据占了绝大多数，所以最终得到的W值使得数据几乎全部预测为$-1$，这就导致了nan值的出现。最终选择最优的口袋算法结果：

| accuracy | precision | recall  |    F     |
| :------: | :-------: | :-----: | :------: |
|  0.844   | 0.528571  | 0.23125 | 0.321739 |

### 四、思考题

1. 有什么其他的手段可以解决数据集非线性可分的问题？
   - 可以通过跑多个PLA，然后从得到的多个结果当中选取一个最优的结果或者对多个结果进行加权处理得到一个新的结果，也就相当于对输入做了n个逻辑回归，得到n个输出结果W，也就是“单层感知器”，之后可以再对这n个结果W进行m个逻辑回归，就能得到更多的更准确的结果，也就是“单隐层神经网络”
   - 通过某种事先选择的非线性映射把实例由低维空间映射到一个高维控件，将非线性可分问题转化为线性可分问题来处理，并在这个高维空间中寻找最优分类超平面。
   - 梯度下降法：每进行一次迭代，都寻找最优的值，然后再进行迭代，一步步下去最终得到一个最优解，不过这个最优解可能不是全局最优而可能是局部最优。
   - 支持向量机：定义为特征空间上的间隔最大的线性分类器，最终可转化为一个凸二次规划问题的求解。对于非线性可分数据，可以通过映射到一个特征空间，使数据变为线性可分，同时找到最大的线性分类器进行二分。
2. 请查询相关资料，解释为什么要用这四种评测指标，各自的意义是什么。
   - Accuracy(准确率)：所有样本中预测正确的数量/所有样本数。可以评价算法的分类程度如何，准确率越高，意味着算法的分类程度越高，应用到测试数据当中的时候能够更好地分类数据，主要关注的是算法得到的整体结果的好坏程度。

   - Precision(精确率)：原本为正预测为正的样本数/所有预测为正的样本数。预测为正的情况下有多少是预测正确的，主要关注的是正结果中，有多少是正真自己想要的。比如分类两种水果，一种苹果一种橘子，我只想要橘子，但是计算机帮我分类的时候，可能会把部分苹果预测为橘子，分到了橘子堆里，这个时候我只要关注的是橘子堆里有多少个正真的橘子而已，因为我只关注橘子，所以精确率是用于关注某二分类中某一类的结果。

   - Recall(召回率)：原本为正预测为正的样本数/所有原本为正的样本数。这个和精确率类似，同样使用上面的例子，我可能同时还关注了所有橘子有多少个正确分到了橘子堆里了，因为我可能设定了所有预测为苹果的都直接销毁，那么橘子预测正确率就很重要了，如果这个召回率低了，就意味着会浪费很多橘子被销毁。所以这同样是个关注二分类中某一类的指标，因为二分类问题中很多时候都是其中的一类是更具关注度的。

   - F1(F值)：公式：
     $$
     F=\frac{2*precision*recall}{precision+recall}
     $$
     可以看出衡量的是精确率和召回率的综合指标，从上面的举例就可以看出，关注点都在橘子，这个时候我不仅希望精确率高，同时也希望召回率高，但其实许多情况下两者是矛盾的，为了平衡这两者，我可以选择F值来作为综合的指标。

