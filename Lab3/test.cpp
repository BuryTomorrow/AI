#include<iostream>
#include<vector>
#include<fstream>
#include<string>
#include<cmath>
using namespace std;

string PATH = "E:/HomeWork/Grade_3_Last/AI/Lab3/";
struct Matrix{   //存储矩阵X，以及每个样本对应额label 
	vector <double> x;
	double label;
};

vector <Matrix> train;
vector <Matrix> val;
vector <Matrix> test;
Matrix w_result;
int K=0; 
/* 
TP:本来为+1，预测为+1
FN:本来为+1，预测为-1
TN:本来为-1，预测为-1
FP:本来为-1，预测为+1 
accuracy:	准确率：(TP+TN)/ALL 
precision:	精确率: (TP)/(TP+FP)
recall:		召回率: (TP)/(TP+FN)
f1:			F值: 	
*/
double TP,FN,TN,FP;
void pretreat(string file,vector <Matrix> &set);
void PLA_initial();
double multi(Matrix &X,Matrix &W);
int main(){
	pretreat("thur78train.csv",train);
	pretreat("thur78test.csv",test);
	PLA_initial();
	
	cout<<"\n 最终W为：( ";
	for(int i=0;i<w_result.x.size();i++){
		cout<<w_result.x[i];
		if(i!=w_result.x.size()-1)cout<<" , ";
	}
	cout<<" )\n";
	cout<<endl<<" 预测结果为 ："<<endl;
	for(int i=0;i<test.size();i++){
		cout<<" 第 " <<i<<" 个向量 ：( ";
		for(int j=1;j<test[i].x.size();j++){
			cout<<test[i].x[j];
			if(j!=test[i].x.size()-1) cout<<" , ";
		}
		cout<<" ) : "<<multi(w_result,test[i])<<endl;
//		cout<<" 实际label为： "<<train[i].label<<endl; 
//		cout<<" 预测为 ："<<multi(w_result,train[i])<<endl<<endl;
	} 
	return 0;
}

void pretreat(string file,vector<Matrix> &set){
	ifstream f(PATH+file);
	string data;
	while(getline(f,data)){
		Matrix X;
		X.x.push_back(1.0);
		int start=0,end=data.find(",",start);
		while(end!=-1){
			string sub=data.substr(start,end-start);
			start=end+1;
			end=data.find(",",start);
			double x=strtod(sub.c_str(),NULL);
			X.x.push_back(x);
		}
		X.label=(strtod(data.substr(start,data.size()-start).c_str(),NULL));
		set.push_back(X);
	}
	
}

double multi(Matrix &X,Matrix &W){
	double m=0;
	for(int i=0;i<X.x.size();i++)
		m+=X.x[i]*W.x[i];
//	cout<<" m== "<<m<<endl;
	if(m>0) return 1.0;
	else if(m<0)	return -1.0;
	else return 0;
}

void PLA_initial(){
	
	for(int i=0;i<train[0].x.size();i++) w_result.x.push_back(1);
	bool flag = false; 
	while(!flag){
		for(int i=0;i<train.size();i++){
			if(multi(train[i],w_result)==train[i].label){
				if(i==train.size()-1) flag = true;
				continue;
			}  	//如果预测正确则找下一个样本 
			for(int j=0;j<w_result.x.size();j++){	//预测错误则更新 
				w_result.x[j]+=train[i].label*train[i].x[j];
			}
//			K++;
//			cout<<"W"<<K<<" 为 ：(";
//			for(int j=0;j<w_result.x.size();j++){
//				cout<<w_result.x[j];
//				if(j!=w_result.x.size()-1)cout<<",";
//			}
//			cout<<")\n";
			break; 
		}
	}
}
