#include<iostream>
#include<vector>
#include<fstream>
#include<string>
#include<cmath>
using namespace std;

string PATH = "E:/HomeWork/Grade_3_Last/AI/Lab3/lab3数据/";
struct Matrix{   //存储矩阵X，以及每个样本对应额label 
	vector <double> x;
	double label;
};

vector <Matrix> train;
vector <Matrix> val;
vector <Matrix> test;
Matrix w_result;
/* 
TP:本来为+1，预测为+1
FN:本来为+1，预测为-1
TN:本来为-1，预测为-1
FP:本来为-1，预测为+1 
accuracy:	准确率：(TP+TN)/ALL 
precision:	精确率: (TP)/(TP+FP)
recall:		召回率: (TP)/(TP+FN)
f1:			F值: 	
*/
double TP,FN,TN,FP;
void pretreat(string file,vector <Matrix> &set);
void PLA_initial();
void predict();

int main()
{
	pretreat("train.csv",train);
	pretreat("val.csv",val);
	//pretreat("test.csv",test);
	PLA_initial();
	predict(); 
	
	return 0;
}

void pretreat(string file,vector<Matrix> &set){
	ifstream f(PATH+file);
	string data;
	while(getline(f,data)){
		Matrix X;
		X.x.push_back(1.0);
		int start=0,end=data.find(",",start);
		while(end!=-1){
			string sub=data.substr(start,end-start);
			start=end+1;
			end=data.find(",",start);
			double x=strtod(sub.c_str(),NULL);
			X.x.push_back(x);
		}
		X.label=(strtod(data.substr(start,data.size()-start).c_str(),NULL));
		set.push_back(X);
	}
}

double multi(Matrix X,Matrix W){
	double m=0;
	for(int i=0;i<X.x.size();i++)
		m+=X.x[i]*W.x[i];
	if(m>0) return 1.0;
	if(m<0)	return -1.0;
	return 0;
}

void PLA_initial(){
	for(int i=0;i<train[0].x.size();i++) w_result.x.push_back(0);
	int itera = 9999;//迭代次数
	while(itera--){
		for(int i=0;i<train.size();i++){
			if(multi(train[i],w_result)==train[i].label) continue; 	//如果预测正确则找下一个样本 
			for(int j=0;j<w_result.x.size();j++){	//预测错误则更新 
				w_result.x[j]+=train[i].label*train[i].x[j];
			}
			break; 
		}
	}
}

void predict(){
	TP=0;FN=0;TN=0;FP=0;
	for(int i=0;i<val.size();i++){
		if(val[i].label==1){
			if(multi(val[i],w_result)==1) TP++;
			else FN++;
		}else{
			if(multi(val[i],w_result)==-1) TN++;
			else FP++;
		}
	}
	double precision=(TP)/(TP+FP),recall=TP/(TP+FN);
	cout<<"\nTP = "<<TP<<" , FN = "<<FN<<", TN = "<<TN<<" , FP = "<<FP<<endl;
	cout<<"accuracy : "<<(TP+TN)/(TP+FP+TN+FN)<<endl;
	cout<<"precision : "<<precision<<endl;
	cout<<"recall : "<<recall<<endl;
	cout<<"F1 : "<<2*precision*recall/(precision+recall)<<endl;
}
