#include<iostream>
#include<vector>
#include<fstream>
#include<string>
#include<cmath>
#include<stdlib.h> 
#include<time.h>
#include<algorithm>
using namespace std;

string PATH = "E:/HomeWork/Grade_3_Last/AI/Project/regression/";
struct Matrix {   //存储矩阵X，以及每个样本对应额label 
	vector <double> feature;//每个样本的特征 
	double T;//真实单车数
};

int weekday = 6, Date = 1;//初始的日期

int feature_count;//输入的特征数量 
int hide_count = 80;//隐藏节点数 
double t = 0.5;

vector <Matrix> Data;//总数据
vector <Matrix> test;//测试数据
vector <Matrix> train; //训练集 
vector <Matrix> val; //验证集
vector <double> Max;//记录最大最小值用于归一化
vector <double> Min;

void pretreat(string file, vector <Matrix> &set);//预处理
//对数据进行归一化处理
void normal_train(vector <Matrix> &set);
void normal_test(vector <Matrix> &set);	  
int main()
{
	pretreat("train.csv", Data);
	normal_train(Data);//归一化 
	pretreat("test.csv", test);
	normal_test(test);//归一化 
	
	ofstream f1(PATH + "data/data.csv", ios::trunc);
	//f1 << "年份,月份,星期,是否周末,hr,weathersit,temp,atemp,hum,windspeed,cnt" << endl;
	for (int i = 0; i < Data.size(); i++) {
		for (int j = 0; j < Data[i].feature.size(); j++) {
			f1 << Data[i].feature[j] << ",";
			//cout << Data[i].feature[j] << "\t";
		}
		f1 << Data[i].T << endl;
		//cout << Data[i].T << endl;
	}
	ofstream f2(PATH + "data/data2.csv", ios::trunc);
	//f2 << "年份,月份,星期,是否周末,hr,weathersit,temp,atemp,hum,windspeed,cnt" << endl;
	for (int i = 0; i < test.size(); i++) {
		for (int j = 0; j < test[i].feature.size(); j++) {
			f2 << test[i].feature[j] << ",";
		}
		f2 << test[i].T << endl;
	}
	system("pause");
	return 0;
}

void pretreat(string file, vector<Matrix> &set) { //预处理 
	ifstream f(PATH + file);
	string data;
	int i = -1;
	while (getline(f, data)) {
		i++;
		if (i == 0) {//跳过第一行的特征名字数据
			continue;
		}
		Matrix X;
		int start = 0, end = data.find(",", start), j = -1;
		bool flag = true;
		while (end != -1) {
			j++;
			string sub = data.substr(start, end - start);
			start = end + 1;
			end = data.find(",", start);
			if (j == 0 && strtod(sub.c_str(), NULL) == 0) {//如果为第一列的属性且其值为0，则舍去
				flag = false;
				break;
			}
			else if (j == 1) {//若为日期那一列属性，则分为年份、月份、日期、星期,2011/1/1是星期6
				int sta = 0, en = sub.find("/", sta), k = -1;
				sub += "/";
				while (en != -1) {
					k++;
					string s2 = sub.substr(sta, en - sta);
					sta = en + 1;
					en = sub.find("/", sta);
					double date = strtod(s2.c_str(), NULL);
					if (k == 0) {//年份,将2011和2012映射成两位二进制，10代表2011,01代表2012
						for (int p = 2011; p <= 2012; p++) {
							if (p == (date)) X.feature.push_back(1);
							else X.feature.push_back(0);
						}
					}
					else if (k == 1) {//月份,将12个月映射成12位二进制
						for (int p = 1; p <= 12; p++) {
							if (p == int(date)) X.feature.push_back(1);
							else X.feature.push_back(0);
						}
						//X.feature.push_back(date);
					}
					else {//日.主要用于判别星期几，将星期映射成7位2进制
						if (date != Date) {//意味着到了下一天,星期数加1
							Date = date;
							weekday = weekday % 7 + 1;
						}
						//X.feature.push_back(weekday);
						for (int p = 1; p <= 7; p++) {
							if (p == weekday) X.feature.push_back(1);
							else X.feature.push_back(0);
						}
					}
				}
			}
			else if (j == 2) {//若为时间属性，则将时间映射成24位二进制表示，每一位代表一个小时
				double x = strtod(sub.c_str(), NULL);
				for (int p = 0; p <= 23; p++) {
					if (p == int(x)) X.feature.push_back(1);
					else X.feature.push_back(0);
				}
			}
			else if (j == 3) {//天气种类
				double x = strtod(sub.c_str(), NULL);
				if (sub == "?") x = 1;
				for (int p = 1; p <= 4; p++) {
					if (p == (int)x) X.feature.push_back(1);
					else X.feature.push_back(0);
				}
			}
			else if (j != 0) {//其他属性直接保留
				double x = strtod(sub.c_str(), NULL);
				X.feature.push_back(x);
			}
		}
		X.feature.push_back(1.0);//加入偏置量
		X.T = (strtod(data.substr(start, data.size() - start).c_str(), NULL));
		if (flag) set.push_back(X);
	}
}

//训练数据的归一化
void normal_train(vector <Matrix> &set) {
	//记录每一列的最大最小值
	for (int j = 0; j < set[0].feature.size() - 1; j++) {//初始化
		Max.push_back(set[0].feature[j]);
		Min.push_back(set[0].feature[j]);
	}
	//找到最大最小值
	for (int i = 0; i < set.size(); i++) {
		for (int j = 0; j < set[i].feature.size() - 1; j++) {
			Max[j] = max(Max[j], set[i].feature[j]);
			Min[j] = min(Min[j], set[i].feature[j]);
		}
	}
	//根据最大最小值进行归一化
	for (int i = 0; i < set.size(); i++) {
		for (int j = 0; j < set[i].feature.size() - 1; j++) {
			set[i].feature[j] = (set[i].feature[j] - Min[j]) / (Max[j] - Min[j]);
		}
	}
}
//测试集的归一化
void normal_test(vector <Matrix> &set) {
	//根据最大最小值进行归一化
	for (int i = 0; i < set.size(); i++) {
		for (int j = 0; j < set[i].feature.size() - 1; j++) {
			set[i].feature[j] = (set[i].feature[j] - Min[j]) / (Max[j] - Min[j]);
		}
	}
}
