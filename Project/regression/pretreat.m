function [data] = pretreat(Data)
  data=[];
  weekday = 6;
  Date = 1;
  for j=2:length(Data)
     temp=[];
     for i=1:length(Data{1,j})
         if char(Data{1,1}{i}) == '0' %去除instant为0的数据
            continue;
         end
         %将字符串转换为数字：
         a = 0;
         if char(Data{1,j}{i}) == '?'
             a = 0;
         else
             a = str2num(char(Data{1,j}{i}));
         end
         %数据预处理
         b = [];
         if j == 2 %年份
             for p=2011:2012
                if p==a
                   b=[b 1]; 
                else
                   b=[b 0]; 
                end
             end
         elseif j==3 %月份
            for p = 1:12
               if p == a
                   b=[b 1];
               else
                   b=[b 0];
               end
            end
         elseif j==4 %日期
            if(a~=Date) %判断是否需要更新星期
                Date = a;
                weekday = mod(weekday,7)+1;
            end
            for p=1:7
               if p == weekday
                   b=[b 1]; 
               else 
                   b=[b 0];
               end
            end
         elseif j==5 %时间
            for p=0:23
               if p == a
                   b=[b 1]; 
               else 
                   b=[b 0];
               end
            end
         elseif j==6 %天气
            if a == 0
               a = 1; 
            end
            for p=1:4
               if p == a
                   b=[b 1]; 
               else 
                   b=[b 0];
               end
            end
         else
            b=[b a];
         end
         temp=[temp;b];
     end
     data=[data temp];
  end
  
