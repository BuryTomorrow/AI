clear;
fileTrain = fopen('E:/HomeWork/Grade_3_Last/AI/Project/regression/train.csv');
TrainData = textscan(fileTrain,'%s %s %s %s %s %s %s %s %s %s %s', 'delimiter',[',','/'],'headerlines',1);
fileTest = fopen('E:/HomeWork/Grade_3_Last/AI/Project/regression/test.csv');
TestData = textscan(fileTest,'%s %s %s %s %s %s %s %s %s %s %s', 'delimiter',[',','/'],'headerlines',1);
fclose('all');
TrainData = pretreat(TrainData); %数据预处理
TestData = pretreat(TestData);

%归一化处理
[row col] = size(TrainData);
for i=1:col-1
    amax = max (max(TrainData(:,i)),max(TestData(:,i)) );
    amin = min( min(TrainData(:,i)),min(TestData(:,i)) );
    if amax == amin
        amin = 0;
    end
    TrainData(:,i) = (TrainData(:,i)-amin)/(amax-amin); 
    TestData(:,i) = (TestData(:,i)-amin)/(amax-amin); 
end

dlmwrite('E:/HomeWork/Grade_3_Last/AI/Project/regression/data.csv', TrainData);
