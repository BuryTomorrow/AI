#include<iostream>
#include<vector>
#include<fstream>
#include<string>
#include<cmath>
#include<stdlib.h> 
#include<time.h>
#include<algorithm>
using namespace std;

string PATH = "E:/HomeWork/Grade_3_Last/AI/Project/regression/";
//存储矩阵X，以及每个样本对应额label 
struct Matrix {
	vector <double> feature;//每个样本的特征 
	double T;//真实单车数
};


int weekday = 6, Date = 1;//初始的日期 
vector <Matrix> Data;//总数据
vector <Matrix> test;//测试数据
vector <Matrix> train; //训练集 
vector <Matrix> val; //验证集
vector <double> Max;//记录最大最小值用于归一化
vector <double> Min;

//预处理读取数据
void pretreat(string file, vector <Matrix> &set);
//对数据进行归一化处理
void normal_train(vector <Matrix> &set);
void normal_test(vector <Matrix> &set);	   
//划分数据 
void divide(int k); 
//神经网络函数，传入训练集，隐藏层层数，各层之间使用的激活函数类型，各层权重,各隐藏层节点数
void NN(vector<Matrix> &train, vector<string> &type,vector<vector <vector <double> > > &Weight );
//初始化全部权重
void init(vector<vector <vector <double> > > &Weight, vector<int> &Num);
//前向传递过程
void forward_pass(Matrix &sample, vector<string> &type, vector<vector <vector <double> > > &Weight, vector<vector<double> > &Output);
//反向传递过程
void backward_pass(Matrix &sample, vector<string> &type, vector<vector <vector <double> > > &Weight,vector<vector<double> > &Output);
//激活函数
double activate(string &type, double &input);
//求导函数
double derivative(string &type, double &input);
double loss_function(vector<Matrix> &set, vector<string> &type, vector<vector <vector <double> > > &Weight);//计算损失
void setResult(vector<Matrix> &set, vector<string> &type, vector<vector <vector <double> > > &Weight ,string count);

double LEARN = 0.00003;
int main()
{
	pretreat("train.csv", Data);
	normal_train(Data);//归一化 
	pretreat("test.csv", test);
	pretreat("Bike-Sharing-Dataset/val.csv", val);
	normal_test(test);//归一化 
	normal_test(val);//归一化 
	divide(11); //划分数据
	int C = train[0].feature.size() - 1;
	//这里可以任意修改各层的激活函数
	string types[] = { "tanh","tanh","f(x)=x" };
	//这里可以任意修改各层的节点数
	int num[] = { C,100,100,1 };
	vector<vector <vector <double> > > Weight;
	vector<string> type;
	vector<int> Num;
	for (int i = 0; i < sizeof(types)/sizeof(types[0]); i++) {
		type.push_back(types[i]);
	}
	for (int i = 0; i < sizeof(num) / sizeof(num[0]); i++) {
		Num.push_back(num[i]);
	}
	//先初始化所有的权重
	init(Weight, Num);

	ofstream f_train(PATH + "loss/train_loss.csv", ios::trunc);
	ofstream f_val(PATH + "loss/val_loss.csv", ios::trunc);
	ofstream f_1(PATH + "得到新解的迭代次数.txt", ios::trunc);
	int itera = 0,count = 1;
	double last = 0, old = 3500;
	while (1) {//迭代
		itera++;
		NN(train, type, Weight);
		double now = loss_function(val,type,Weight);
		cout << "itera == " << itera << "\t\t" << LEARN <<"\t\t"<<loss_function(train, type, Weight)<<"\t\t"<<now<< endl;
		//f_train << loss_function(train, type, Weight) << endl;
		f_val << now << endl;
		if (itera<400) {
			if (itera %30==0) {
					LEARN *= 0.97;
			}
			if (now < last&&last - now < 1) {
					LEARN *= 1.05;
			}
		}
		else {
			if (now >= last || itera % 50 == 0) {
					LEARN *= 0.85;
			}
			if (now < last&&last - now < 0.1) {
					LEARN *= 1.02;
			}
		}
		if (old-now>100) {
			string s = "";
			int w = count;
			count++;
			while (w) {
				int w2 = w % 10;
				w /= 10;
				char c = char(w2 + '0');
				s = c + s;
			}
			f_1 << "第"<<s<<"份新数据的迭代次数为："<<itera << endl;
			setResult(test,type,Weight,s);
			old = now;
		}
		last = now;
	}
	system("pause");
	return 0;
}
//数据预处理
void pretreat(string file, vector<Matrix> &set) { //预处理 
	ifstream f(PATH + file);
	string data;
	int i = -1;
	while (getline(f, data)) {
		i++;
		if (i == 0) {//跳过第一行的特征名字数据
			continue;
		}
		Matrix X;
		int start = 0, end = data.find(",", start), j = -1;
		bool flag = true;
		
		while (end != -1) {
			j++;
			string sub = data.substr(start, end - start);
			start = end + 1;
			end = data.find(",", start);
			if (j == 0 && strtod(sub.c_str(), NULL) == 0) {//如果为第一列的属性且其值为0，则舍去
				flag = false;
				break;
			}
			else if (j == 1) {//若为日期那一列属性，则分为年份、月份、日期、星期,2011/1/1是星期6
				int sta = 0, en = sub.find("/", sta), k = -1;
				sub += "/";
				double nian, yue, ri;
				while (en != -1) {
					k++;
					string s2 = sub.substr(sta, en - sta);
					sta = en + 1;
					en = sub.find("/", sta);
					double date = strtod(s2.c_str(), NULL);
					if (k == 0) {//年份,将2011和2012映射成两位二进制，10代表2011,01代表2012
						for (int p = 2011; p <= 2012; p++) {
							if (p == (date)) X.feature.push_back(1);
							else X.feature.push_back(0);
						}
						nian = date;
					}
					else if (k == 1) {//月份,将12个月映射成12位二进制
						for (int p = 1; p <= 12; p++) {
							if (p == int(date)) X.feature.push_back(1);
							else X.feature.push_back(0);
						}
						yue = date;
					}
					else {//日.主要用于判别星期几，将星期映射成7位2进制
						if (date != Date) {//意味着到了下一天,星期数加1
							Date = date;
							weekday = weekday % 7 + 1;
						}
						ri = date;
						for (int p = 1; p <= 7; p++) {
							if (p == weekday) X.feature.push_back(1);
							else X.feature.push_back(0);
						}
						if (weekday >= 6) X.feature.push_back(1);//周末
						else X.feature.push_back(0);
					}
				}
				//添加节假日属性
				if (nian == 2011) { //2011年节假日
					if (yue == 1 && ri == 17 || yue == 2 && ri == 21 || yue == 4 && ri == 15 || yue == 5 && ri == 30 ||
						yue == 7 && ri == 4 || yue == 9 && ri == 5 || yue == 10 && ri == 10 || yue == 11 && ri == 11 ||
						yue == 11 && ri == 24 || yue == 12 && ri == 26) {
						X.feature.push_back(1);
					}
					else {
						X.feature.push_back(0);
					}
				}
				else{//2012年节假日
					if (yue == 1 && ri == 2 || yue == 1 && ri == 16 || yue == 2 && ri == 20 || yue == 4 && ri == 16 ||
						yue == 5 && ri == 28 || yue == 7 && ri == 4 || yue == 9 && ri == 3 || yue == 10 && ri == 8 ||
						yue == 11 && ri == 12 || yue == 11 && ri == 22 || yue == 12 && ri == 25) {
						X.feature.push_back(1);
					}
					else {
						X.feature.push_back(0);
					}
				}
				//添加季节属性
				double season = 1;
				if (yue <= 2 || yue == 3 && ri <= 20 || yue == 12 && ri >= 21) season = 1;
				if (yue == 3 && ri >= 21 || yue == 4 || yue == 5 || yue == 6 && ri <= 20) season = 2;
				if (yue == 6 && ri >= 21 || yue == 7 || yue == 8 || yue == 9 && ri <= 22) season = 3;
				if (yue == 9 && ri >= 23 || yue == 10 || yue == 11 || yue == 12 && ri <= 20) season = 4;
				for (int p = 1; p <= 4; p++) {
					if (p == season) X.feature.push_back(1);
					else X.feature.push_back(0);
				}
			}
			else if (j == 2) {//若为时间属性，则将时间映射成24位二进制表示，每一位代表一个小时
				double x = strtod(sub.c_str(), NULL);
				for (int p = 0; p <= 23; p++) {
					if (p == int(x)) X.feature.push_back(1);
					else X.feature.push_back(0);
				}
			}
			else if (j == 3) {//天气种类
				double x = strtod(sub.c_str(), NULL);
				if (sub == "?") x = 1;
				for (int p = 1; p <= 4; p++) {
					if (p == (int)x) X.feature.push_back(1);
					else X.feature.push_back(0);
				}
			}
			else if (j != 0) {//其他属性直接保留
				double x = strtod(sub.c_str(), NULL);
				X.feature.push_back(x);
			}
		}
		X.feature.push_back(1.0);//加入偏置量
		X.T = (strtod(data.substr(start, data.size() - start).c_str(), NULL));
		if (flag) set.push_back(X);
	}
}
//训练数据的归一化
void normal_train(vector <Matrix> &set) {
	//记录每一列的最大最小值
	for (int j = 0; j < set[0].feature.size() - 1; j++) {//初始化
		Max.push_back(set[0].feature[j]);
		Min.push_back(set[0].feature[j]);
	}
	//找到最大最小值
	for (int i = 0; i < set.size(); i++) {
		for (int j = 0; j < set[i].feature.size() - 1; j++) {
			Max[j] = max(Max[j], set[i].feature[j]);
			Min[j] = min(Min[j], set[i].feature[j]);
		}
	}
	//根据最大最小值进行归一化
	for (int i = 0; i < set.size(); i++) {
		for (int j = 0; j < set[i].feature.size() - 1; j++) {
			set[i].feature[j] = (set[i].feature[j] - Min[j]) / (Max[j] - Min[j]);
		}
	}
}
//测试集的归一化
void normal_test(vector <Matrix> &set) {
	//根据最大最小值进行归一化
	for (int i = 0; i < set.size(); i++) {
		for (int j = 0; j < set[i].feature.size() - 1; j++) {
			set[i].feature[j] = (set[i].feature[j] - Min[j]) / (Max[j] - Min[j]);
		}
	}
}
//划分数据
void divide(int k) {
	for (int i = 0; i<Data.size(); i++) {
		//if (i % 25 == k) val.push_back(Data[i]);
		train.push_back(Data[i]);
	}
}
//BP神经网络
void NN(vector<Matrix> &train,vector<string> &type, vector<vector <vector <double> > > &Weight) {
	srand(time(0));
	for (int n = 0; n < train.size(); n++) {
		int r = rand() % 1000;
		//if (r < 300||r>900) continue;
		vector<vector<double>> Output;
		//前向传递
		forward_pass(train[n], type, Weight, Output); 
		//反向更新
		backward_pass(train[n], type, Weight,Output);
	}
}
//初始化全部权重
void init(vector<vector <vector <double> > > &Weight, vector<int> &Num) {
	srand(time(0));
	for (int k = 0; k < Num.size()-1; k++) {
		vector<vector<double>>temp1; //第k层到第k+1层权重
		for (int i = 0; i < Num[k] + 1; i++) { //前一层添加偏置项
			vector<double>temp2;
			for (int j = 0; j < Num[k + 1]; j++) {
				int a = rand() % 1000 - 500;
				double b = double(a)/500 ;
				temp2.push_back(b);
			}
			temp1.push_back(temp2);
		}
		Weight.push_back(temp1);
	}
}
//前向传递
void forward_pass(Matrix &sample, vector<string> &type, vector<vector <vector <double> > > &Weight, vector<vector<double> > &Output) {
	//各层前向传递输出,第k层的输出为前k-1层输出*权重再经过激活函数
	for (int k = 0; k < Weight.size(); k++) {
		vector<double> temp; //第k+1层的输出结果
		for (int j = 0; j < Weight[k][0].size(); j++) {
			double Ij = 0;
			for (int i = 0; i < Weight[k].size(); i++) {
				double in;
				if (k == 0) in = sample.feature[i]; //如果为输入层到隐藏层，那么输入就是样本属性
				else in = Output[k-1][i];  //否则就是上一层的输出结果
				Ij += in*Weight[k][i][j];
			}
			double Out = activate(type[k], Ij);//经过激活函数输出
			temp.push_back(Out);
		}
		if (k != Weight.size() - 1) temp.push_back(1.0);//添加偏置项,但最后一层不需要添加
		Output.push_back(temp);
	}
}
//反向传递过程
void backward_pass(Matrix &sample, vector<string> &type, vector<vector <vector <double> > > &Weight, vector<vector<double> > &Output) {
	vector<vector<double> > Error;//记录每一层的误差
	vector<double> t;
	t.push_back(sample.T - Output[Output.size() - 1][0]);
	Error.push_back(t);
	for (int k = Output.size() - 1; k >= 0; k--) { //从最后一个输出开始反向传递
		int lastError = Output.size() - k - 1;
		//记录传递的误差值
		vector<double> temp;
		for (int i = 0; i < Weight[k].size(); i++) {
			double e = 0 ,out= 0;//output_i表示第i个节点的输出，如果是输入层则为属性
			if (k == 0) out = sample.feature[i];
			else out = Output[k - 1][i];
			for (int j = 0; j < Weight[k][i].size(); j++) { //L层第i个节点的误差等于第L+1层所有j节点误差之和
				e += Error[lastError][j]*Weight[k][i][j];
				Weight[k][i][j] += LEARN*Error[lastError][j] * out;//更新权重
			}
			if(k>0) e *= derivative(type[k - 1], Output[k - 1][i]); 
			temp.push_back(e);
		}
		Error.push_back(temp);
	}
}
//激活函数
double activate(string &type, double &input) {
	if (type == "sigmoid") {
		return 1.0 / (1.0 + exp(0.0 - input));
	}
	if (type == "tanh") {
		return 2.0 / (1 + exp(0.0 - 2 * input)) - 1;
	}
	if (type == "f(x)=x") {
		return input;
	}
	return 0;
}
//求导函数
double derivative(string &type, double &output) {
	if (type == "sigmoid") {
		return output*(1.0 - output);
	}
	if (type == "tanh") {
		return 1.0 - output*output;
	}
	if (type == "f(x)=x") {
		return 1.0;
	}
	return 1.0;
}
//验证集mse计算
double loss_function(vector<Matrix> &set, vector<string> &type, vector<vector <vector <double> > > &Weight) {
	double mse = 0, n = set.size();
	for (int i = 0; i < set.size(); i++) {
		vector<vector<double>> Output;
		forward_pass(set[i], type, Weight, Output);
		double output = Output[Output.size() - 1][0];
		mse += (set[i].T - output)*(set[i].T - output);
	}
	return mse / n;
}
//测试集结果预测
void setResult(vector<Matrix> &set, vector<string> &type, vector<vector <vector <double> > > &Weight ,string count) {
	ofstream f(PATH + "4_v" + count+".csv", ios::trunc);
	for (int i = 0; i < set.size(); i++) {
		vector<vector<double>> Output;
		forward_pass(set[i], type, Weight, Output);
		int output = Output[Output.size() - 1][0];
		if (output<0) output = 0;
		f << output << endl;
	}
	f.close();
}
