#include<iostream>
#include<vector>
#include<fstream>
#include<string>
#include<cmath>
#include<stdlib.h> 
#include<time.h>
#include<algorithm>
using namespace std;

string PATH = "E:/HomeWork/Grade_3_Last/AI/Project/regression/";
//存储矩阵X，以及每个样本对应额label 
struct Matrix {
	vector <double> feature;//每个样本的特征 
	double T;//真实单车数
};

double LEARN = 1;
int weekday = 6, Date = 1;//初始的日期 
vector <Matrix> Data;//总数据
vector <Matrix> test;//测试数据
vector <Matrix> train; //训练集 
vector <Matrix> val; //验证集
vector <double> Max;//记录最大最小值用于归一化
vector <double> Min;

//预处理读取数据
void pretreat(string file, vector <Matrix> &set);
//对数据进行归一化处理
void normal_train(vector <Matrix> &set);
void normal_test(vector <Matrix> &set);	   
//w矩阵的初始化，使用随机赋值
void init_w();
//划分数据 
void divide(int k); 
//神经网络函数，传入训练集，隐藏层层数，各层之间使用的激活函数类型，各层权重,各隐藏层节点数
void NN(vector<Matrix> &train, vector<string> &type,vector<vector <vector <double> > > &Weight );
//初始化全部权重
void init(vector<vector <vector <double> > > &Weight, vector<int> &Num);
//前向传递过程
void forward_pass(Matrix &sample, vector<string> &type, vector<vector <vector <double> > > &Weight, vector<vector<double> > &Output);
//反向传递过程
void backward_pass(Matrix &sample, vector<string> &type, vector<vector <vector <double> > > &Weight,vector<vector<double> > &Output);
//激活函数
double activate(string &type, double &input);
//求导函数
double derivative(string &type, double &input);
double loss_function(vector<Matrix> &set, vector<string> &type, vector<vector <vector <double> > > &Weight);//计算损失
void setResult();


int main()
{
	//pretreat("train.csv", Data);
	//normal_train(Data);//归一化 
	//pretreat("test.csv", test);
	//normal_test(test);//归一化 
	//divide(2); //划分数据
	Matrix y;
	y.feature.push_back(1);
	y.feature.push_back(0);
	y.feature.push_back(1);
	y.feature.push_back(1);
	y.T=1;
	train.push_back(y);
	
	int C = train[0].feature.size() - 1;
	//这里可以任意修改各层的激活函数
	string types[] = { "sigmoid","f(x)=x" };
	//这里可以任意修改各层的节点数
	int num[] = { C,2,1 };

	vector<vector <vector <double> > > Weight;
	vector<string> type;
	vector<int> Num;
	for (int i = 0; i < sizeof(types)/sizeof(types[0]); i++) {
		type.push_back(types[i]);
	}
	for (int i = 0; i < sizeof(num) / sizeof(num[0]); i++) {
		Num.push_back(num[i]);
	}
	vector <vector <double> > InputToHide;
	vector <vector <double> > HideToOutput;
	vector <double> a;
	a.push_back(0.2);
	a.push_back(-0.3);
	InputToHide.push_back(a);
	vector <double> b;
	b.push_back(0.4);
	b.push_back(0.1);
	InputToHide.push_back(b);
	vector <double> c;
	c.push_back(-0.5);
	c.push_back(0.2);
	InputToHide.push_back(c);
	vector <double> d;
	d.push_back(-0.4);
	d.push_back(0.2);
	InputToHide.push_back(d);
	Weight.push_back(InputToHide);
	//隐藏层权重处理的时候要比隐藏层节点数量多一个，因为需要加入偏置项 
	vector<double> temp1;
	temp1.push_back(-0.3);
	HideToOutput.push_back(temp1);
	vector<double> temp2;
	temp2.push_back(-0.2);
	HideToOutput.push_back(temp2);
	vector<double> temp3;
	temp3.push_back(0.1);
	HideToOutput.push_back(temp3);
	Weight.push_back(HideToOutput);
	
//	for(int k=0;k<Weight.size();k++){
//		for(int i=0;i<Weight[k].size();i++){
//			for(int j=0;j<Weight[k][i].size();j++){
//				cout<<"i == "<<i << " j == "<< j << "  "<<Weight[k][i][j]<<endl;
//			}
//		}
//		cout<<endl;
//	}
	
	NN(train,type,Weight);
	
	for(int k=0;k<Weight.size();k++){
		for(int i=0;i<Weight[k].size();i++){
			for(int j=0;j<Weight[k][i].size();j++){
				cout<<"i == "<<i << " j == "<< j << "  "<<Weight[k][i][j]<<endl;
			}
		}
		cout<<endl;
	}
	
	system("pause");
	return 0;
}
//数据预处理
void pretreat(string file, vector<Matrix> &set) { //预处理 
	ifstream f(PATH + file);
	string data;
	int i = -1;
	while (getline(f, data)) {
		i++;
		if (i == 0) {//跳过第一行的特征名字数据
			continue;
		}
		Matrix X;
		int start = 0, end = data.find(",", start), j = -1;
		bool flag = true;
		while (end != -1) {
			j++;
			string sub = data.substr(start, end - start);
			start = end + 1;
			end = data.find(",", start);
			if (j == 0 && strtod(sub.c_str(), NULL) == 0) {//如果为第一列的属性且其值为0，则舍去
				flag = false;
				break;
			}
			else if (j == 1) {//若为日期那一列属性，则分为年份、月份、日期、星期,2011/1/1是星期6
				int sta = 0, en = sub.find("/", sta), k = -1;
				sub += "/";
				while (en != -1) {
					k++;
					string s2 = sub.substr(sta, en - sta);
					sta = en + 1;
					en = sub.find("/", sta);
					double date = strtod(s2.c_str(), NULL);
					if (k == 0) {//年份
						if (date == 2011.0) X.feature.push_back(1); //2011年
						else X.feature.push_back(2);//2012年
					}
					else if (k == 1) {//月份
									  //cout << date << " ";
						X.feature.push_back(date);//直接放入月份
					}
					else {//日
						  //cout << date << " " << endl;
						if (date != Date) {//意味着到了下一天,星期数加1
							Date = date;
							weekday = weekday % 7 + 1;
						}
						//X.feature.push_back(weekday);
						if (weekday >= 6) X.feature.push_back(1);
						else X.feature.push_back(0);
					}
				}
			}
			else if (j != 0) {//其他属性直接保留
				double x = strtod(sub.c_str(), NULL);
				X.feature.push_back(x);
			}
		}
		X.feature.push_back(1.0);//加入偏置量
		X.T = (strtod(data.substr(start, data.size() - start).c_str(), NULL));
		if (flag) set.push_back(X);
	}
}
//训练数据的归一化
void normal_train(vector <Matrix> &set) {
	//记录每一列的最大最小值

	for (int j = 0; j < set[0].feature.size() - 1; j++) {//初始化
		Max.push_back(set[0].feature[j]);
		Min.push_back(set[0].feature[j]);
	}
	//找到最大最小值
	for (int i = 0; i < set.size(); i++) {
		for (int j = 0; j < set[i].feature.size() - 1; j++) {
			Max[j] = max(Max[j], set[i].feature[j]);
			Min[j] = min(Min[j], set[i].feature[j]);
		}
	}
	//根据最大最小值进行归一化
	for (int i = 0; i < set.size(); i++) {
		for (int j = 0; j < set[i].feature.size() - 1; j++) {
			set[i].feature[j] = (set[i].feature[j] - Min[j]) / (Max[j] - Min[j]);
		}
	}
}
//测试集的归一化
void normal_test(vector <Matrix> &set) {
	//根据最大最小值进行归一化
	for (int i = 0; i < set.size(); i++) {
		for (int j = 0; j < set[i].feature.size() - 1; j++) {
			set[i].feature[j] = (set[i].feature[j] - Min[j]) / (Max[j] - Min[j]);
		}
	}
}
//划分数据
void divide(int k) {
	for (int i = 0; i<Data.size(); i++) {
		if (i % 5 == k) val.push_back(Data[i]);
		else train.push_back(Data[i]);
	}
}

void NN(vector<Matrix> &train,vector<string> &type, vector<vector <vector <double> > > &Weight) {
	for (int n = 0; n < train.size(); n++) {
		vector<vector<double>> Output;
		//前向传递
		forward_pass(train[n], type, Weight, Output);
		for(int i=0;i<Output.size();i++){
			for(int j=0;j<Output[i].size();j++){
				cout<<"i== "<<i<< "  j==  "<< j<< " output == "<<Output[i][j]<<endl;
			}
		}
		//反向更新
		backward_pass(train[n], type, Weight,Output);
	}
}
//初始化全部权重
void init(vector<vector <vector <double> > > &Weight, vector<int> &Num) {
	int setTime = 99;
	for (int k = 0; k < Num.size()-1; k++) {
		vector<vector<double>>temp1; //第k层到第k+1层权重
		for (int i = 0; i < Num[k] + 1; i++) { //前一层添加偏置项
			vector<double>temp2;
			for (int j = 0; j < Num[k + 1]; j++) {
				srand(time(0) + setTime);
				setTime = (setTime + 9) % 100;
				int a = rand() % 100 - 50;
				double b = double(a) / 50.0;
				temp2.push_back(b);
			}
			temp1.push_back(temp2);
		}
		Weight.push_back(temp1);
	}
}
//前向传递
void forward_pass(Matrix &sample, vector<string> &type, vector<vector <vector <double> > > &Weight, vector<vector<double> > &Output) {
	//各层前向传递输出,第k层的输出为前k-1层输出*权重再经过激活函数
	for (int k = 0; k < Weight.size(); k++) {
		vector<double> temp; //第k+1层的输出结果
		for (int j = 0; j < Weight[k][0].size(); j++) {
			double Ij = 0;
			for (int i = 0; i < Weight[k].size(); i++) {
				double in;
				if (k == 0) in = sample.feature[i]; //如果为输入层到隐藏层，那么输入就是样本属性
				else in = Output[k-1][i];  //否则就是上一层的输出结果
				//if(k==0) cout<<in<<endl;
				Ij += in*Weight[k][i][j];
			}
			double Out = activate(type[k], Ij);//经过激活函数输出
			temp.push_back(Out);
		}
		if (k != Weight.size() - 1) temp.push_back(1.0);//添加偏置项,但最后一层不需要添加
		Output.push_back(temp);
	}
}
//反向传递过程
void backward_pass(Matrix &sample, vector<string> &type, vector<vector <vector <double> > > &Weight, vector<vector<double> > &Output) {
	vector<vector<double> > Error;//记录每一层的误差
	vector<double> t;
	t.push_back(sample.T - Output[Output.size() - 1][0]);
	Error.push_back(t);
	for (int k = Output.size() - 1; k >= 0; k--) { //从最后一个输出开始反向传递
		int lastError = Output.size() - k - 1;
		//记录传递的误差值
		vector<double> temp;
		for (int i = 0; i < Weight[k].size(); i++) {
			double e = 0 ,out= 0;//output_i表示第i个节点的输出，如果是输入层则为属性
			if (k == 0) out = sample.feature[i];
			else out = Output[k - 1][i];

			for (int j = 0; j < Weight[k][i].size(); j++) { //L层第i个节点的误差等于第L+1层所有j节点误差之和
				e += Error[lastError][j]*Weight[k][i][j];
				Weight[k][i][j] += LEARN*Error[lastError][j] * out;//更新权重
				cout<<LEARN*Error[lastError][j] * out<<"   ";
			}
			if(k>0) e *= derivative(type[k - 1], Output[k - 1][i]); 
			temp.push_back(e);
			cout<<endl;
		}
		cout<<endl;
		Error.push_back(temp);
	}
}
//激活函数
double activate(string &type, double &input) {
	if (type == "sigmoid") {
		return 1.0 / (1.0 + exp(0.0 - input));
	}
	if (type == "tanh") {
		return 2.0 / (1 + exp(0.0 - 2 * input)) - 1;
	}
	if (type == "f(x)=x") {
		return input;
	}
	return 0;
}
//求导函数
double derivative(string &type, double &output) {
	if (type == "sigmoid") {
		return output*(1.0 - output);
	}
	if (type == "tanh") {
		return 1.0 - output*output;
	}
	if (type == "f(x)=x") {
		return 1.0;
	}
	return 1.0;
}

double loss_function(vector<Matrix> &set, vector<string> &type, vector<vector <vector <double> > > &Weight) {
	double mse = 0, n = set.size();
	for (int i = 0; i < set.size(); i++) {
		vector<vector<double>> Output;
		forward_pass(set[i], type, Weight, Output);
		double output = Output[Output.size() - 1][0];
		mse += (set[i].T - output)*(set[i].T - output);
	}
	return mse / n;
}


void setResult() {
	ofstream f(PATH + "15352013_caizejie.txt", ios::trunc);
	for (int i = 0; i < test.size(); i++) {
		//double o = forward_pass(test[i]);
		//int output = o;
		//if (output<0) output = 0;
		//f << output << endl;
	}
	f.close();
}
