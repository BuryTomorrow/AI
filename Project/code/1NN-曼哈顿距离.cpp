#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
using namespace std;


/*KNN*/
vector<double>train_data[50000];
vector<double>test_data[50000];
int row;
int test_row;
int real_col;
double pre_(string tmp2);//将type_A等预编码成1,2,3,4,5 

void Predict();

  int main(){
  	real_col=13;
  	ifstream ss;
  	ss.open("E:/大三/饶阳辉人工智能理论课件+实验课件/Exercise/Project/binary_classification/train.csv");
  	
  	string tmp;
  	row=0;
  	int mm=0;
  	while(getline(ss,tmp)){
  		int col=0;
  		int j=0;
  		stringstream tmp_ss;
  		for(int k=0;k<tmp.length();k++){
  			if(tmp[k]==','){
  				if(col!=1){
  					double number;
  					tmp_ss<<tmp.substr(j,k-j);
  					tmp_ss>>number;
  					train_data[row].push_back(number);
					j=k+1;
					tmp_ss.clear();
				  }
				else{
					tmp_ss<<tmp.substr(j,k-j);
					string tmp2;
					tmp_ss>>tmp2;
					double x = pre_(tmp2);
					train_data[row].push_back(x);
					j=k+1;
					tmp_ss.clear();
				}
				col++;
  				
			}
		}
		double number;
		tmp_ss<<tmp.substr(j,tmp.length()-1);
		tmp_ss>>number;
		train_data[row].push_back(number);
		row++;
	}
	
	
	ifstream test_ss;
  	test_ss.open("E:/大三/饶阳辉人工智能理论课件+实验课件/Exercise/Project/binary_classification/test.csv");
  	
  	string test_tmp;
  	test_row=0;
  	while(getline(test_ss,test_tmp)){
  		int test_col=0;
  		int j=0;
  		stringstream test_tmp_ss;
  		for(int k=0;k<test_tmp.length();k++){
  			if(test_tmp[k]==','){
  				if(test_col!=1){
  					double test_number;
  					test_tmp_ss<<test_tmp.substr(j,k-j);
  					test_tmp_ss>>test_number;
  					test_data[test_row].push_back(test_number);
					j=k+1;
					test_tmp_ss.clear();
				  }
				else{
					test_tmp_ss<<test_tmp.substr(j,k-j);
					string test_tmp2;
					test_tmp_ss>>test_tmp2;
					double x = pre_(test_tmp2);
					test_data[test_row].push_back(x);
					j=k+1;
					test_tmp_ss.clear();
				}
				test_col++;
			}
		}
		double test_number;
		test_tmp_ss<<test_tmp.substr(j,test_tmp.length()-1);
		test_tmp_ss>>test_number;
		test_data[test_row].push_back(test_number);
		test_row++;
	}
	Predict();
	return 0;
  }
  
  double pre_(string tmp2){
  	if(tmp2=="type_A")
  		return 1;
  	else if(tmp2=="type_B")
  		return 2;
  	else if(tmp2=="type_C")
  		return 3;	
  	else if(tmp2=="type_D")
  		return 4;
  	else if(tmp2=="type_E")
  		return 5;
  	return 0;
  }


  int Predict_label(int n){
  	int label=0;
	double min_distance = 999999;
	for(int i=0;i<row;i++){
		double tmp_distance = 0;
		for(int j=0;j<real_col;j++){
			tmp_distance+=fabs((train_data[i][j]-test_data[n][j]));//曼哈顿距离 
		}
		if(min_distance>tmp_distance){
				min_distance = tmp_distance;
				label = train_data[i][real_col];
		}
	}
	return label;
  } 
  
  void Predict(){
  	ofstream result_ss;
	result_ss.open("E:/大三/饶阳辉人工智能理论课件+实验课件/Exercise/Project/binary_classification/最终.txt");
  	int truth =0;
  	for(int i=0;i<test_row;i++){
  		cout<<i <<" "; 
  		int label = Predict_label(i);
//  		if(label==test_data[i][real_col])
//  			truth++;
  		result_ss<<label <<endl;
	  }
	result_ss.close();
  }
  

