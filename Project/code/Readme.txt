二元分类代码：运行1NN-曼哈顿距离.cpp
	K=1，曼哈顿距离的KNN
	将ss路径和test_ss路径改成相应的train、test即可

三元分类代码：放在三元代码文件夹中
	1. 把MulLabelTrain.ss、MulLabelTest.ss放在同一目录下
	
2. 先执行deal_data.cpp，再执行deal_data_again.cpp，最后执行LR.cpp即可


回归代码：代码名字regression.cpp
	将第11行的数据集目录修改，使用的是绝对路径
	然后直接运行代码，达到设定的验证集mse后会输出结果文件在当前代码目录，
	一般会输出多个结果文件，一般400次迭代后开始输出
	可以执行多次，毕竟随机初始化参数，没有记录种子
	
