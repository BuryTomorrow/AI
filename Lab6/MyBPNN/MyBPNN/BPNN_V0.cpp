#include<iostream>
#include<vector>
#include<fstream>
#include<string>
#include<cmath>
#include<stdlib.h> 
#include<time.h>
#include<algorithm>
using namespace std;

string PATH = "E:/HomeWork/Grade_3_Last/AI/Lab6/BPNN_Dataset/";
struct Matrix {   //存储矩阵X，以及每个样本对应额label 
	vector <double> feature;//每个样本的特征 
	double T;//真实单车数
};

int feature_count;//输入的特征数量 
int hide_count = 11;//隐藏节点数 
double max_cnt = 0, min_cnt = 9999;//单车数量的最大最小值

vector <Matrix> Data;//总数据
vector <Matrix> test;//测试数据
vector <Matrix> train; //训练集 
vector <Matrix> val; //验证集
vector <vector <double> > InputToHide;//输入到隐藏层的权重
vector<double> HideToOutput;//隐藏层到输出层的权重

void pretreat(string file, vector <Matrix> &set);//预处理
void normal(vector <Matrix> &set);//对数据进行归一化处理
//w矩阵的初始化，使用随机赋值 
void init_w();
void divide(int k); //划分数据 
double forward_pass(Matrix &input);//前向传递计算

void neural();//神经网络
double validation();//预测 
double loss_function(vector<Matrix> &set);//计算损失
void setResult();


int main()
{
	pretreat("train.csv", Data);

	divide(2); //划分数据

	feature_count = Data[0].feature.size();//计算特征总数

	init_w();//初始化w

	int itera = 20000;
	while (itera--) {//迭代
		neural();//通过神经网络更新权值
		cout << "itera == " << 20000 - itera << endl;
		cout << loss_function(train) << "\t" << loss_function(val) << endl;
	}
	cout << endl;
	validation();

	system("pause");
	return 0;
}
//初始化权重
void init_w() {
	int count = 100;
	for (int i = 0; i<feature_count; i++) {
		vector <double> b;//第i个特征到所有隐层节点的权重 
		for (int j = 0; j<hide_count; j++) {
			srand(count+ time(0));
			count += 100;
			int a = rand() % 100-50;
			double c = double(a)/100;//使初始输入层到隐藏层的权重随机在-0.5~0.5之间 
			b.push_back(c);
		}
		InputToHide.push_back(b);
	}
	//隐藏层权重处理的时候要比隐藏层节点数量多一个，因为需要加入偏置项 
	for (int i = 0; i<hide_count + 1; i++) {//隐藏层到输出层的权重，其中输出层只有一个，即一维
		srand(count+time(0));
		count += 10;
		int a = rand() % 100;
		double c = double(a)/10;//初始隐藏层到输出层的权重随机在0~10之间
		HideToOutput.push_back(c);
	}
}
//数据预处理
void pretreat(string file, vector<Matrix> &set) { //预处理 
	ifstream f(PATH + file);
	string data;
	int i = 0;
	while (getline(f, data)) {
		if (i == 0) {//跳过第一行的特征名字数据 
			i++;
			continue;
		}
		Matrix X;
		int start = 0, end = data.find(",", start);
		int j = -1;
		while (end != -1) {
			j++;
			string sub = data.substr(start, end - start);
			start = end + 1;
			end = data.find(",", start);
			if (j == 0 || j == 1 || j == 3) {//跳过需要舍去的特征 
				continue;
			}
			double x = strtod(sub.c_str(), NULL);
			int intx = x*10000.0;
			if (j == 10||j==12) { //如果是第10列temp属性或第12列hum属性，其值有两位小数点，先乘以100,再转换为2进制保存
				intx /= 100;
				//将属性映射到7个位上，以0或1做结果
				for (int i = 0; i < 7; i++) {
					if (intx % 2) X.feature.push_back(1);
					else X.feature.push_back(0);
					cout << intx % 2 << endl;
					intx /= 2;
				}
			}else if (j == 11||j==13) {//如果是第11列atem属性或第13列wind属性，有小数点后4位，所以先乘10000，再以10进制大小保存4位
				for (int i = 0; i < 4; i++) {
					if (intx % 10 == 0 && intx != 0) X.feature.push_back(intx % 10);
					else X.feature.push_back(0);
					cout << intx % 10 << endl;
					intx /= 10;
				}
			}
			else {
				X.feature.push_back(x);
			}
		}
		X.feature.push_back(1.0);//加入偏置量
		X.T = (strtod(data.substr(start, data.size() - start).c_str(), NULL));
		set.push_back(X);
	}
}

void normal(vector <Matrix> &set) {
	//记录每一列的最大最小值
	vector <double> Max;
	vector <double> Min;
	for (int j = 0; j < set[0].feature.size()-1; j++) {//初始化
		Max.push_back(set[0].feature[j]);
		Min.push_back(set[0].feature[j]);
	}
	//找到最大最小值
	for (int i = 0; i < set.size(); i++) {
		for (int j = 0; j < set[i].feature.size()-1; j++) {
			Max[j] = max(Max[j], set[i].feature[j]);
			Min[j] = min(Min[j], set[i].feature[j]);
		}
	}
	//根据最大最小值进行归一化
	for (int i = 0; i < set.size(); i++) {
		for (int j = 0; j < set[i].feature.size()-1; j++) {
			set[i].feature[j] = (set[i].feature[j] - Min[j]) / (Max[j] - Min[j]);
		}
	}
	//找到单车数量的最大最小值
	
	for (int i = 0; i < set.size(); i++) {
		max_cnt = max(max_cnt, set[i].T);
		min_cnt = min(min_cnt, set[i].T);
	}
	//更新单车数量
	for (int i = 0; i < set.size(); i++) {
		set[i].T = (set[i].T - min_cnt) / (max_cnt - min_cnt);
	}
}

void divide(int k) {//划分数据
	for (int i = 0; i<Data.size(); i++) {
		if (i > Data.size() - 21) {
			val.push_back(Data[i]);
			continue;
		}
		if (i % 5 == k) val.push_back(Data[i]);
		else train.push_back(Data[i]);
	}
}

//前向传递过程
double forward_pass(Matrix &input) {
	double Output = 0;//最终输出结果
	//计算输入层到隐藏层的输出Oj：
	vector <double> Out;//记录隐藏层输出的值
	for (int j = 0; j < InputToHide[0].size(); j++) {
		double Ij = 0;//用于计算隐藏层的输入
		for (int i = 0; i < input.feature.size(); i++) {//节点i求和所有特征属性xj的权重
			Ij += input.feature[i] * InputToHide[i][j];
		}
		double o = 1.0 / (1.0 + exp(0.0 - Ij));//得到第i个隐藏节点的输出，使用sigmoid函数作为激活函数
		Out.push_back(o);
	}
	Out.push_back(1.0);//在隐藏节点最后加入一项偏置项
	//计算隐藏层到输出层：激活函数f(h)=h；
	for (int i = 0; i < HideToOutput.size(); i++) {
		//cout << HideToOutput[i] << "\t\t" << Out[i] << endl;
		Output += HideToOutput[i] * Out[i];
	}
	//cout << Output << endl;
	return Output;
}

void neural() {
	vector <vector <double> > Err_hide;//隐藏层误差梯度，直接存储隐藏层每个节点的误差
	for (int i = 0; i<InputToHide.size(); i++) {
		vector <double> b;//第i个特征到所有隐层节点的权重梯度
		for (int j = 0; j<InputToHide[i].size(); j++) {
			b.push_back(0);
		}
		Err_hide.push_back(b);
	}
	vector <double> Err_output;//输出层误差,存储Wj的更新梯度
	for (int i = 0; i < HideToOutput.size(); i++) {
		Err_output.push_back(0);
	}
	for (int k = 0; k < train.size(); k++) {
		double Output = 0;//最终输出结果
		/************************************前向传递过程-开始*************************************/
		//计算输入层到隐藏层的输出Oj：
		vector <double> Out;//记录隐藏层输出的值
		for (int j = 0; j < InputToHide[0].size(); j++) {
			double Ij = 0;//用于计算隐藏层的输入
			for (int i = 0; i < train[k].feature.size(); i++) {//节点j求和所有特征属性xi的权重
				Ij += train[k].feature[i] * InputToHide[i][j];
			}
			double o = 1.0 / (1.0 + exp(0.0 - Ij));//得到第i个隐藏节点的输出，使用sigmoid函数作为激活函数
			Out.push_back(o);
		}
		Out.push_back(1.0);//在隐藏节点最后加入一项偏置项
		//计算隐藏层到输出层：激活函数f(h)=h；
		for (int j = 0; j < HideToOutput.size(); j++) {
			Output += HideToOutput[j] * Out[j];
		}
		/************************************前向传递过程-结束*************************************/

		/************************************后向传递过程-开始*************************************/

		//计算输出层误差:err=(T-O),其梯度为err*Oj
		for (int j = 0; j < HideToOutput.size(); j++) {
			Err_output[j] += (train[k].T - Output)*Out[j];
		}
		//隐藏层反向传递到输出层，梯度为Oj(1-Oj)Err_output*wjk
		for (int i = 0; i < InputToHide.size(); i++) {
			for (int j = 0; j < InputToHide[i].size(); j++) { //特征属性i到第j个隐藏节点的梯度为Wij=Oj(1-Oj)Err_output*xi
				Err_hide[i][j] += Out[j] * (1 - Out[j])*(train[k].T - Output)*HideToOutput[j] * train[k].feature[i];
			}
		}
	}

	//学习率
	double n = 0.01;
	//更新W
	double m = train.size();
	for (int i = 0; i < HideToOutput.size(); i++) {
		HideToOutput[i] += n*Err_output[i] / m;
	}
	for (int i = 0; i<InputToHide.size(); i++) {
		for (int j = 0; j<InputToHide[i].size(); j++) {
			InputToHide[i][j] += n*Err_hide[i][j]/m;
		}
	}
	/************************************后向传递过程-结束*************************************/
}

double loss_function(vector<Matrix> &set) {
	double mse = 0, n = set.size();
	for (int i = 0; i < set.size(); i++) {
		double output = forward_pass(set[i]);
		mse += (set[i].T - output)*(set[i].T - output);
	}
	return mse / n;
}
//验证最后20天数据
double validation() {
	for (int i = Data.size() - 21; i < Data.size(); i++) {
		double output = forward_pass(Data[i]);
		cout << int(output) << "\t" << Data[i].T << endl;
	}
	return 0;
}

double multi(Matrix &X, vector <double> &W) { //向量相乘 
	double m = 0;
	for (int i = 0; i<X.feature.size(); i++)
		m += X.feature[i] * W[i];
	return m;
}

void setResult() {
	ofstream f(PATH + "15352013_caizejie.txt", ios::trunc);
}