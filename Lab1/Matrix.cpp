#include<iostream>
#include<iomanip>
#include<fstream>
#include<cstring>
#include<string>
#include<vector>
#include<unordered_set>
#include<algorithm>
using namespace std;

string LIST = "E:/HomeWork/Grade_3_Last/AI/Lab1/lab1 数据集处理/";
vector <string> train_s;	//储存所有文本
vector <string> words;		//储存所有不重复的词 
vector <double> words_count;//记录各个训练文本的词数 
double tf[4000][4000];		//储存tf矩阵，方便计算tf-idf矩阵 

void read(){
	fstream f(LIST+"text.txt");
	char data[222];
	while(f.getline(data,222,'\n')!=NULL){				//读取文本并切割储存 
		string s=data;
		s=s.substr(s.find("\t",s.find("\t",0)+1)+1);	//利用文本中的缩进进行来分割文本 
		train_s.push_back(" "+s+" ");					//在文本前后添加空格方便后续切割以及匹配 
	}
	f.close();											//读取文本完毕，开始切割文本中的词并储存 
	for(int i=0;i<train_s.size();i++){
		int start=1,end=train_s[i].find(" ",start);		//因为前面添加了一个空格，所以从1号位开始查找空格 
		double count=0;									//用于记录各个文本中词汇数量 
		while(end!=-1){
			string substr=train_s[i].substr(start,end-start);
			start=end+1;								//从下一个空格后开始继续查找空格 
			end=train_s[i].find(" ",start);
			//利用find函数查找该词是否已经存在，如果不存在就储存到vector中 
			if(find(words.begin(),words.end(),substr)==words.end()) words.push_back(substr);
			count++;
		}
		words_count.push_back(count);					 
	}
}

void One_hot(){
	ofstream f_one(LIST+"onehot.txt",ios::trunc);
	ofstream f_sma(LIST+"smatrix.txt",ios::trunc);
	f_sma<<train_s.size()<<endl;						//onehot三元顺序表的总行数 
	f_sma<<words.size()<<endl;							//onehot三元顺序表的总列数 
	int sum=0;											//计算onehot三元顺序表中有值的总个数 
	vector <int> row;									//记录有值的行 
	vector <int> col;									//记录有值的列，与行的记录同时使用，确保匹配 
	for(int i=0;i<train_s.size();i++){
		for(int j=0;j<words.size();j++){
			if(j!=0) f_one<<" ";
			if(train_s[i].find(" "+words[j]+" ")==-1){	//前后加空格确保查找的词不是子字符串 
				f_one<<0;
			}else{
				f_one<<1;
				sum++;
				row.push_back(i);						//记录onehot三元顺序表 
				col.push_back(j);
			}
		}
		f_one<<endl;
	}
	f_sma<<sum<<endl;									//输出onehot三元顺序表 
	for(int i=0;i<row.size();i++)
		f_sma<<row[i]<<" "<<col[i]<<" "<<1<<endl;
	f_one.close();
	f_sma.close();
}

void TF(){
	for(int i=0;i<train_s.size();i++){
		for(int j=0;j<words.size();j++){
			double count = 0;
			int start=train_s[i].find(" "+words[j]+" ",0);
			while(start!=-1){							//查找该词在文本中出现的次数 
				count++;
				start = train_s[i].find(" "+words[j]+" ",start+1);
			}
			tf[i][j]=count/words_count[i];				//记录tf矩阵用于后面计算tf-idf矩阵 
		}
	}
	ofstream f(LIST+"TF.txt",ios::trunc);				//输出TF矩阵 
	for(int i=0;i<train_s.size();i++){					
		for(int j=0;j<words.size();j++){
			if(j!=0) f<<" ";
			f<<tf[i][j];
		}
		f<<endl;
	}
	f.close();
}

void TF_IDF(){
	ofstream f(LIST+"TF_IDF.txt",ios::trunc);
	vector <double> user_count;
	for(int i=0;i<words.size();i++){					//计算各个词汇被使用的文本数 
		double count=0;
		for(int j=0;j<train_s.size();j++)
			if(train_s[j].find(" "+words[i]+" ")!=-1)	count++;
		user_count.push_back(count);
	}
	double all=train_s.size();
	for(int i=0;i<train_s.size();i++){					//输出TF-IDF矩阵 
		for(int j=0;j<words.size();j++){
			if(j!=0) f<<" ";
			if(tf[i][j]!=0)	f<<tf[i][j]*log(all/(1+user_count[j]))/log(2);
			else	f<<0;
		}
		f<<endl;
	}
	f.close();
} 

int main()
{
	read(); 
	One_hot();
	TF();
	TF_IDF();
	return 0;
}
