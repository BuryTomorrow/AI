#include<iostream>
#include<iomanip>
#include<fstream>
#include<cstring>
#include<string>
#include<vector>
#include<unordered_set>
#include<algorithm>
using namespace std;

struct Node{
	int row;
	int col;
	int val;
};

string LIST = "E:/HomeWork/Grade_3_Last/AI/Lab1/lab1 数据集处理/";
vector <Node> node1;	//用于存储三元顺序表中的数据 
vector <Node> node2;	//用于存储三元顺序表中的数据 
vector <Node> result;	//用于存储三元顺序表加和后的结果 
int r_num,c_num,v_num;	//记录总行数，总列数，有值总数 

void read(string s,int type){				//进行文件读取，s为文件目录，type用于区分文件 
	fstream f(s);
	f>>r_num>>c_num>>v_num;					//先读取开头的总行数，总列数，有值总数 
	int r,c,v;
	while(f>>r>>c>>v){						//依次读取行、列、值 
		Node n;
		n.row=r;
		n.col=c;
		n.val=v;
		if(type==1) node1.push_back(n);		//储存三元顺序表		
		else node2.push_back(n);
	}
	f.close();
}

void add(){									//两个三元顺序表求和 
	int i=0,j=0;
	while(i<node1.size()&&j<node2.size()){	//归并 
		if(node1[i].row*10000+node1[i].col<node2[j].row*10000+node2[j].col){
			result.push_back(node1[i++]);
		}
		else if(node1[i].row*10000+node1[i].col>node2[j].row*10000+node2[j].col){
			result.push_back(node2[j++]);
		}
		else{
			node1[i].val+=node2[j++].val;
			result.push_back(node1[i++]);
		}
	}
	//补充剩余的三元顺序表 
	while(i<node1.size()) result.push_back(node1[i++]);
	while(j<node2.size()) result.push_back(node2[j++]);
	
	ofstream f(LIST+"add_result.txt",ios::trunc);
	f<<r_num<<endl<<c_num<<endl<<result.size()<<endl;
	for(int k=0;k<result.size();k++) f<<result[k].row<<" "<<result[k].col<<" "<<result[k].val<<endl;
	f.close();
}

int main()
{
	read(LIST+"A.txt",1);
	read(LIST+"B.txt",2);
	add();
	return 0;
}
